FROM python:3.8

ARG K8S_VERSION=v1.21.5
ARG HELM_VERSION=v3.7.0

ENV VALUE_NORMALIZER_RULE_SPREADSHEET_ID 1v4CO-BjMwx2qih7KE_73BVLMNMIIVXop2siaH9DDZ2o

WORKDIR /
ADD setup.py /
ADD README.md /

RUN mkdir rdv_africa_data \
  && pip install --no-cache-dir --extra-index-url=https://gitlab.com/api/v4/projects/29120915/packages/pypi/simple/ -e .

WORKDIR /rdv_africa_data

ADD rdv_africa_data .

CMD [ "python3" , "start_processing.py"]
