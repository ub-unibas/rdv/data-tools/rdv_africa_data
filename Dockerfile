FROM python:3.8

ARG K8S_VERSION=v1.21.5
ARG HELM_VERSION=v3.7.0

RUN apt-get update && apt-get install -y libmagickwand-dev exempi

WORKDIR /
ADD setup.py /
ADD README.md /

RUN python -m pip install --upgrade pip

# Increase Memory for ImageMagick to process images
RUN sed -e 's/memory" value="256MiB/memory" value="1GiB/g' -i /etc/ImageMagick-6/policy.xml
RUN sed -e 's/disk" value="1GiB/disk" value="10GiB/g' -i /etc/ImageMagick-6/policy.xml

RUN mkdir digiaccess

RUN mkdir rdv_africa_data \
  && pip install --no-cache-dir --extra-index-url=https://gitlab.switch.ch/api/v4/projects/2597/packages/pypi/simple/ -e .

WORKDIR /rdv_africa_data

ADD rdv_africa_data .

CMD [ "python3" , "start_processing.py"]
