#RDV Data Processor 
lädt, processiert und mappt Daten anhand einer in Spreadsheets definierten Zuordnung

Wo nötig werden die Klassen überschrieben, siehe insbesondere unter data_loader

## before local run
* google api service-file is needed to access AP-Folder on Spreadsheets (reference it in /config/ap_config.yaml)
* redis-cache (normally socket /run/redis/redis.sock ), see config https://gitlab.com/ub-unibas/py-utilities/ubunibas_cache/-/blob/master/ubunibas_cache/cfg/ and
  https://redis.io/topics/quickstart
    * don't install via package manager but install manually
    * make a configuration so that redis uses the socket /run/redis/redis.sock as it is specified so in https://gitlab.com/ub-unibas/py-utilities/ubunibas_cache/. Copy redis.conf to another configuration (for example myredis.conf) file and change
      ```
      unixsocket /run/redis/redis.sock
      unixsocketperm 700
      ``` 
    * `sudo mkdir /run/redis`
    * give the good permissions to that folder `sudo chmod go+w /run/redis`
    * start the server with `redis-server myredis.conf`
* mongo-store (localhost:27017, configs for mongo need to be adapted: https://gitlab.com/ub-unibas/py-utilities/ubunibas_mongo/-/tree/main/ubunibas_mongo/cfg)
  * start mongo with `sudo systemctl start mongod`
* elastic
  * use a local elastic
  * or tunnel to a cluster 
* yaml-config config/ap_config.yaml needs to be manipulated


## setup

* create venv `python3 -m venv ./venv`
* Install requirements `./venv/bin/python3 -m pip install -r requirements.txt`
* Prepare for running `./venv/bin/python3 setup.py install`


## Run via docker-compose



## Class descriptions

###  Config

Yaml-Config für Prozess spezifische Steuerung

### RDVProcessData

Steuert das Zusammenspiel der folgenden Prozesse

### RDVDataLoader

lädt alle benötigten Daten für eine Institution und bereitet diese für die Weiterverarbeitung auf (wird je Datengeber (teils) überschrieben), können z.B das Zusammenführen von csv und Thesaurus sein, wie bei der BAB oder das Abholen der OAI-Daten für SLSP

braucht get_data Methode, die ein Dict oder eine Liste aller Records zurückgibt, die weiterverarbeitet werden sollen

### RDVGDMapping

lädt die MappingKonfiguration (sprechende Namen) von Google Spreadsheets und führt diese mit dem Datenmodell (ES-Defintionen) zusammen

braucht get_esmapping_field: Umwandlung zwischen ES-Datenmodell und Feldzuordnung im Mapping

braucht get_mapping_rule: Mapping-Regel, kann ein Feld aber auch eine komplexere Methode sein

set_no_esmapping falls automatisch die noch nicht definierten Felder ins Mapping-Spreadsheet aufgenommen werden sollen

erzeugt automatisch fehlende Mapping-Spreadsheets und die zu mappenden Felder

### RDVDataMapper

relativ leere Hülle,  wende einfach die Mapping Definition auf die vom DataLoader geholten Daten an

braucht map_data Methode, in der für alle Records eine RDVCHO Klasse aufgerufen wird, die die weitere Verarbeitung übernimmt

### RDVCHO

einzelner Record der prozessiert wird

(wird je Datengeber (teils) überschrieben)

braucht _map_cho_data Methode

### RDVEnrichment

Enrichment Prozesse können auf Ebene der einzelnen RDVCHOs angewandt werden, z.B. Datumsbereinigung oder Koordinaten-Anreicherung

Zuordnung in RDVCHO postprocess_data über key-dict

### RDVIngest

steuert wo die prozessierten Daten geladen werden sollen, z.B. ES oder OpenRefine

# Force code-style checking before committing

Done with pre-commits and flake8 : <https://flake8.pycqa.org/en/latest/user/using-hooks.html>

Needs to run in the console of the venv
```
pre-commit install
```

# Automatically fix coding errors

```
autopep8 --in-place --aggressive *.py
```

# Deploy on K8s with helm

```
helm install rdv-africa-data helm-charts --namespace=ub-digitale-dienste -f helm-charts/helm-values/rdv-africa-data-test.yaml
```

or, to remove

```
helm uninstall rdv-africa-data --namespace=ub-digitale-dienste
```
