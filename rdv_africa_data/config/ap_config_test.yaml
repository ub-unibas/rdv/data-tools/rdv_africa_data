google_drive:
  # environment variable where the credentials for google drive are stored
  service_account_environment_variable: GOOGLE_DOC_API_KEY
  # path to service file to authenticate service account for Google Drive
  #service_file: /afrikaportalunibas-c50b67f4d403.json
mapping:
  # Folder where subproject folders are created
  # https://drive.google.com/drive/u/0/folders/1B3p2C51xU2pGNOtxrVSo_bD9aJK3U1SP
  main_folder: 1B3p2C51xU2pGNOtxrVSo_bD9aJK3U1SP

  # Spreadsheet where datamodel is defined
  # https://docs.google.com/spreadsheets/d/12F0SqvYIexbd7RxVfFjl3ZLfIg3pJZL1vhdM8WPZUBQ
  data_model_spreadsheet: 12F0SqvYIexbd7RxVfFjl3ZLfIg3pJZL1vhdM8WPZUBQ

  # Spreadsheet which is used as template to create new mappings for new institutions
  # https://docs.google.com/spreadsheets/d/1NCLERhBH1mpbZQgmd3QuMw3HmeUOb0VPHANS3-cy9h0
  # Basically contains the headers (first line) and the various tabs
  mapping_template_spreadsheet: 1NCLERhBH1mpbZQgmd3QuMw3HmeUOb0VPHANS3-cy9h0
enrichment:
  # process to normalize date values
  datenormalization:
    # spreadsheets with normalization rules
    # https://docs.google.com/spreadsheets/d/1jRt6S8MceUOH0SZpEQoxGHaCJOcdiuMpPg3FWApmM2g
    rules_spreadsheet: 1jRt6S8MceUOH0SZpEQoxGHaCJOcdiuMpPg3FWApmM2g
  document_types:
    # spreadsheet which maps the document types to a normalized list of document types
    document_types_spreadsheet: 16v_6ibuGDZ9yK2fpPd3GJILAU9m9kwmRgip0A3lP-wg
  gnd_translation:
    # spreadsheet with the english translations of the GND terms used in Afrika-Portal
    gnd_translation_spreadsheet: 17iYbs5mg3vFtRLJKk_Et4C5zXfTplPleIlqNU6fNDkE
  alma_bib_codes:
    # spreadsheet with mapping alma bib code to bib name
    alma_bib_codes_spreadsheet: 1bFrUbc7PitPTlFTA1G-KGT8P2pEHPLcEoqyd24aB3-U
  gnd_enrichment_from_mongo:
    active: true
  geo_enrichment:
    active: true
    geonames_username: liowalter

  # if we want to retrieve images from media server
  retrieve_images: true
elasticsearch:
  # es_host to ingest data
  # es_host: sb-ues5.swissbib.unibas.ch:8080
  # es_host: es01:9200
  es_host: sb-ues5.swissbib.unibas.ch:8080
  # main alias name to combine all dataset indices
  index: afrikaportal-test

mongo:
  # host: mongodb://localhost # this is mongo in the sidecar container
  # port: 27017
  host: mongodb://dd-db5.ub.unibas.ch
  port: 29017
  cache: True
  store_empty: [ True, null, 0, "", [ ], { } ]

redis:
  host: localhost # this is the redis which is included in the pod, not persistent
  port: 6379
  db: 3
  cache: True
  exp_time: 90000000 # in seconds
  store_empty: [True, null, 0, "", [], {}]


# the cache to store retrieved coordinates from geonames
redis_geonames:
  host: ub-redis.ub.unibas.ch
  port: 6379
  db: 3
  cache: True
  exp_time: 90000000 # in seconds, make 2.8 years


media_server:
  host: https://media.ub.unibas.ch

datasets:
  updates_processing_spreadsheet: 1HCcp63DGUku4ZB-mFlmB7jo0YaGbSb5vnEO67ci4O7U
  # data from Basel IZ ID
  bsizdata:
    project:
      project: bsizdata
      project_gaccount: martinreisacher1983@gmail.com
    process:
      # The date from when you want to harvest the records if you do incremental harvest
      incremental: true
      last_harvest_date: 2024-05-02
      # If you want to reharvest the data from the oai provider
      reharvest: false

      # place to define Mapping for marc XML data
      # https://docs.google.com/spreadsheets/d/1oJ6C6VDANE8Rzs8P3bfBpp51x0taG9laR_fe_wQLc5o
      # that's where the librarian works
      marc_process_spreadsheet: 1oJ6C6VDANE8Rzs8P3bfBpp51x0taG9laR_fe_wQLc5o

      # place to define mapping from simplified marc XML Data to Elasticsearch Datamodel
      # https://docs.google.com/spreadsheets/d/1hakKfXGZKXBKQPclGsn4mkmz-BExORz0fnxVQMu0l3Y
      # that's where the result is written
      mapping_spreadsheet: 1uKkVI9qKiePpj_PNvFax-t2fu1AMHOHxkGKMN3FjNhU
    ingest:
      # name for dataset index
      sub_index: swisscovery-test
      # prefix for id to have unique ids within all indices
      index_prefix: swisscovery-test
  bablibrary:
    project:
      project: bablibrary
      project_gaccount: martinreisacher1983@gmail.com
    process:
      # place for XML Thesaurus from BAB
      #thesaurus_filename: /home/martin/PycharmProjects/rdv_africaslsp_data/rdv_africa_data/project_data/bab_library/BABZentralthesaurus.xml
      mapping_spreadsheet:
      media_server_collection: bablibrary-010
      gnd_enrichment_spreadsheet: 1r4pKA29pDox6vRgrnkUPIKNm10VYfhPU-3CzII6_u2Y
    ingest:
      sub_index: bablibrary-test
      index_prefix: bablibrary-test
  babarchive:
    project:
      project: babarchive
      project_gaccount: martinreisacher1983@gmail.com
    process:
      # place for XML Thesaurus from BAB
      #thesaurus_filename: /home/martin/PycharmProjects/rdv_africaslsp_data/rdv_africa_data/project_data/bab_library/BABZentralthesaurus.xml
      # we will load all csv files which are within this folder
      mapping_spreadsheet:
      media_server_collection: babarchive-010
      gnd_enrichment_spreadsheet: 1r4pKA29pDox6vRgrnkUPIKNm10VYfhPU-3CzII6_u2Y # same as BAB Library
    ingest:
      sub_index: babarchive-test
      index_prefix: babarchive-test
  mkb:
    project:
      project: mkb
      project_gaccount: martinreisacher1983@gmail.com
    process:
      mapping_spreadsheet:
      media_server_collection: mkb-010
      gnd_enrichment_spreadsheet: 1mNbjp5edr-NwXqvfPcUUOtjkDdFNwY3Bnyz1VmMeY9Y
    ingest:
      sub_index: mkb-test
      index_prefix: mkb-test
  stph:
    project:
      project: stph
      project_gaccount: martinreisacher1983@gmail.com
    process:
      # file where the projects are located
      mapping_spreadsheet:
      # media server collection where the images are located
      # cf. https://ub-basel.atlassian.net/wiki/spaces/AF/pages/2050293774/UB+Medienserver+or+Media+server#GET-Collections
      media_server_collection: stph-010
      gnd_enrichment_spreadsheet: 1OdbG3uKeBNDo3LjxBfkrsab3x7A8DLzRfe1yqKrGlPo
    ingest:
      sub_index: stph-image-test
      index_prefix: stph-image-test
  stph-projects:
    project:
      project: stph-projects
      project_gaccount: martinreisacher1983@gmail.com
    process:
      mapping_spreadsheet:
      gnd_enrichment_spreadsheet: 1OdbG3uKeBNDo3LjxBfkrsab3x7A8DLzRfe1yqKrGlPo
    ingest:
      sub_index: stph-project-test
      index_prefix: stph-project-test
  stph-films:
    project:
      project: stph-films
      project_gaccount: martinreisacher1983@gmail.com
    process:
      mapping_spreadsheet:
      gnd_enrichment_spreadsheet: 1OdbG3uKeBNDo3LjxBfkrsab3x7A8DLzRfe1yqKrGlPo
    ingest:
      sub_index: stph-film-test
      index_prefix: stph-film-test
  mission21:
    project:
      project: mission21
      project_gaccount: martinreisacher1983@gmail.com
    process:
      mapping_spreadsheet:
      gnd_enrichment_spreadsheet: 1O3WHa0jM5YatF-o3xpYLqVJwNX7_trkCXgu1nYjC5rI
    ingest:
      sub_index: mission21-test
      index_prefix: mission21-test
  foerster:
    project:
      project: foerster
      project_gaccount: martinreisacher1983@gmail.com
    process:
      mapping_spreadsheet:
      # media server collection where the images are located
      # cf. https://ub-basel.atlassian.net/wiki/spaces/AF/pages/2050293774/UB+Medienserver+or+Media+server#GET-Collections
      media_server_collection: foerster-010
      gnd_enrichment_spreadsheet: 1BS2G8J5NYDMmcioSHc4QD-fqQamTL-LpkaFkZT4VF_w
    ingest:
      sub_index: foerster-test
      index_prefix: foerster-test
  stph-images:
    media_server_collection: stph-test-001
  bablibrary-images:
    media_server_collection: bablibrary-test-001
  babarchive-images:
    media_server_collection: babarchive-test-001
  mkb-images:
    media_server_collection: mkb-test-001
  foerster-images:
    media_server_collection: foerster-test-001