from rdv_data_helpers_ubit import get_rdv_logger

import os

import yaml


def get_config():
    logger = get_rdv_logger(yaml_config="config/logging.yaml")
    config_with_path = get_config_filename_with_path()

    try:
        with open(config_with_path) as ap_config_fh:
            ap_config = yaml.load(ap_config_fh, Loader=yaml.Loader)
        return ap_config
    except OSError:
        logger.error("Impossible to load configuration")


def get_config_filename_with_path():
    config_file = os.getenv("CONFIG_FILE")
    if config_file is None:
        config_file = "ap_config_local.yaml"
    return "config/" + config_file
