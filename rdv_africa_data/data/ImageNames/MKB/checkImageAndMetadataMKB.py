import os
import csv

count = 0

with open("files.txt") as csvfile:
    files = csv.reader(csvfile, dialect='excel')
    with open("inventarnummer.txt") as csvfile:
        inventory_numbers = csv.reader(csvfile, dialect='excel')
        files_array = list(files)
        inventory_numbers_array = list(inventory_numbers)
        for file in files_array:
            match = False
            inventory_number_from_file = file[0].split('_')[0].replace("-", " ")
            for inventory_number in inventory_numbers_array:
                inv_num = inventory_number[0]
                if(inventory_number_from_file == inv_num):
                    print("there is a match for " + file[0])
                    match = True
                    count = count + 1
                    break
            if (match is False):
                print("no match for " + file[0])

print(count + "files were matched")
