from cache_decorator_redis_ubit import NoCache
from values_normalizer_ubit import DateNormalizerAF, RuleLoadeAF
from values_normalizer_ubit.enrichment_google_spreadsheet import MappingLoader, SpreadsheetEnricher


class RDV_Enrichment:

    def __init__(self, project, logger, config, cachedecorator=NoCache):
        self.logger = logger
        self.cachedecorator = cachedecorator

        gd_service_account_environment_variable = config.get("google_drive", {}).get("service_account_environment_variable")
        date_rules = config.get("enrichment", {}).get("datenormalization", {}).get("rules_spreadsheet")

        date_rules = RuleLoadeAF.load_rules_from_gd(main_rules_ss=date_rules, gd_service_account_environment_variable=gd_service_account_environment_variable,
                                                    gd_cache_decorator=cachedecorator)
        date_normalizer = DateNormalizerAF(rules=date_rules)

        self.enrich_dict = {
            "hidden_dateObject": date_normalizer,
        }

        self.project = project

        gnd_spreadsheet_id = config.get("datasets", {}).get(self.project, {}).get("process", {}).get("gnd_enrichment_spreadsheet", None)

        if gnd_spreadsheet_id:
            spreadsheet_mapping = MappingLoader.load_mapping_from_gd(
                spreadsheet_id=gnd_spreadsheet_id,
                source_column_name="Source term",
                target_column_name="Target ID",
                gd_cache_decorator=NoCache,
            )

            spreadsheet_enricher = SpreadsheetEnricher(mapping=spreadsheet_mapping.mapping)

            self.enrich_dict["fct_topic"] = spreadsheet_enricher

        document_types_spreadsheet_id = config.get("enrichment", {}).get("document_types", {}).get("document_types_spreadsheet", None)

        if document_types_spreadsheet_id:
            spreadsheet_document_types = MappingLoader.load_mapping_from_gd(
                spreadsheet_id=document_types_spreadsheet_id,
                source_column_name="Source term",
                target_column_name="Target term",
                gd_cache_decorator=NoCache,
            )

            spreadsheet_enricher_document_types = SpreadsheetEnricher(mapping=spreadsheet_document_types.mapping)
            self.enrich_dict["fct_mediatype"] = spreadsheet_enricher_document_types

        gnd_translation_spreadsheet_id = config.get("enrichment", {}).get("gnd_translation", {}).get("gnd_translation_spreadsheet", None)

        if gnd_translation_spreadsheet_id:
            spreadsheet_gnd_translation = MappingLoader.load_mapping_from_gd(
                spreadsheet_id=gnd_translation_spreadsheet_id,
                source_column_name="gnd label",
                target_column_name="lcsh labels",
                gd_cache_decorator=NoCache,
            )

            spreadsheet_enricher_gnd_translation = SpreadsheetEnricher(mapping=spreadsheet_gnd_translation.mapping)
            self.enrich_dict["gnd_translation"] = spreadsheet_enricher_gnd_translation

        alma_bib_codes_spreadsheet_id = config.get("enrichment", {}).get("alma_bib_codes", {}).get(
            "alma_bib_codes_spreadsheet", None)

        if alma_bib_codes_spreadsheet_id:
            spreadsheet_alma_bib_codes = MappingLoader.load_mapping_from_gd(
                spreadsheet_id=alma_bib_codes_spreadsheet_id,
                source_column_name="Code",
                target_column_name="Bibliothek",
                gd_cache_decorator=NoCache,
            )

            spreadsheet_enricher_alma_bib_codes = SpreadsheetEnricher(mapping=spreadsheet_alma_bib_codes.mapping)
            self.enrich_dict["852_b_4"] = spreadsheet_enricher_alma_bib_codes

        enrich_gnd_from_mongo = config.get("enrichment", {}).get("gnd_enrichment_from_mongo", {}).get("active", True)
        self.enrich_dict["gnd_enrichment_from_mongo"] = enrich_gnd_from_mongo

        geo_enrichment = config.get("enrichment", {}).get("geo_enrichment", {}).get("active", True)
        self.enrich_dict["geo_enrichment"] = geo_enrichment

    def get_enrichment_process(self, key):
        return self.enrich_dict.get(key)
