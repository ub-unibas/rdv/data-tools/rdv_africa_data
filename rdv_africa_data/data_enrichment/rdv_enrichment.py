from cache_decorator_redis_ubit import CacheDecorator, ShortCacheDecorator
from values_normalizer_ubit import DateNormalizerAF, RuleLoadeAF


class RDV_Enrichment:

    def __init__(self, logger, config, cachedecorator=CacheDecorator):
        self.logger = logger
        self.cachedecorator = cachedecorator

        gd_service_account_environment_variable = config.get("google_drive", {}).get("service_account_environment_variable")
        date_rules = config.get("enrichment", {}).get("date_normalization", {}).get("rules_spreadsheet")

        date_rules = RuleLoadeAF.load_rules_from_gd(main_rules_ss=date_rules, gd_service_account_environment_variable=gd_service_account_environment_variable,
                                                    gd_cache_decorator=CacheDecorator)
        date_normalizer = DateNormalizerAF(rules=date_rules)
        self.enrich_dict = {"hidden_dateObject": date_normalizer}

    def get_enrichment_process(self, key):
        return self.enrich_dict.get(key)
