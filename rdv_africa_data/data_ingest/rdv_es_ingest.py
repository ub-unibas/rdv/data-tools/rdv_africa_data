from elasticsearch import Elasticsearch

from es_ingester_ubit import IngestEs
from rdv_marc_ubit import ALMA_MARC_DUMP, AlmaIZMarcJSONRecord, MarcTransformRule
from cache_decorator_redis_ubit import CacheDecorator, NoCacheDecorator
from gdspreadsheets_ubit import PygExtendedClient, authorize_pyg


class RDVIngest:

    def __init__(self, es_host, data, index, sub_index, index_prefix):
        self.es_host = es_host
        self.data = data
        self.index = index
        self.sub_index = sub_index
        self.index_prefix = index_prefix

    def ingest_data(self):
        africa_data = self.data
        africa_data_with_nested_fields = self.add_facet_data_as_nested_field(africa_data)
        # TODO
        # "body": self.body, "mapping": self.autocomplete_mapping
        mapping = {
            "properties": {
                "keyword_facets": {
                    "type": "nested",
                    "properties": {
                        "facet_name": {
                            "type": "keyword"
                        },
                        "facet_value": {
                            "type": "keyword"
                        }
                    }
                }
            }
        }
        ingest_africa_args = {"es": Elasticsearch(self.es_host, timeout=200), "index_name": self.index,
                              "sub_index": self.sub_index, "index_prefix": self.index_prefix, "mapping": mapping}
        ingest_africa = IngestEs(**ingest_africa_args)
        ingest_africa.load_data(africa_data_with_nested_fields)
        ingest_africa.alias_new_index()

    @staticmethod
    def add_facet_data_as_nested_field(data):
        for id in data:
            record = data[id]
            nested_facets = []
            for field in record:
                if field.startswith("fct"):
                    for value in record[field]:
                        nested_facets.append(
                            {
                                "facet_name": field,
                                "facet_value": value
                            }
                        )
            if nested_facets:
                data[id]["keyword_facets"] = nested_facets
        return data
