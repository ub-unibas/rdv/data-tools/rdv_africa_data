from elasticsearch import Elasticsearch

from es_ingester_ubit import IngestEs


class RDVIngest:

    def __init__(self, es_host, data, index, sub_index, index_prefix):
        self.es_host = es_host
        self.data = data
        self.index = index
        self.sub_index = sub_index
        self.index_prefix = index_prefix

    def ingest_data(self):
        africa_data = self.data
        # TODO
        # "body": self.body, "mapping": self.autocomplete_mapping

        body = {
            "settings": {
                "analysis": {
                    "filter": {
                        "autocomplete_filter": {
                            "type": "edge_ngram",
                            "min_gram": "1",
                            "max_gram": "20"
                        }
                    },
                    "normalizer": {
                        "useLowercase": {
                            "filter": [
                                "lowercase",
                                "asciifolding"
                            ],
                            "type": "custom"
                        }
                    },
                    "analyzer": {
                        "autocomplete": {
                            "filter": [
                                "lowercase",
                                "autocomplete_filter"
                            ],
                            "type": "custom",
                            "tokenizer": "standard"
                        }
                    }
                },
            },
            "mappings": {
                "properties": {
                    "coordinates": {
                        "properties": {
                            "location": {
                                "type": "geo_point"
                            }
                        }
                    },
                    "country_coordinates": {
                        "properties": {
                            "location": {
                                "type": "geo_point"
                            }
                        }
                    },
                    "fct_date": {
                        "type": "date",
                        "ignore_malformed": True,
                        # the fct_date field won't be present if the date doesn't have the good format
                        "format": "yyyy-MM-dd||yyyy"
                    },
                    "processing_time": {
                        "type": "date",
                        "ignore_malformed": True,
                        # the fct_date field won't be present if the date doesn't have the good format
                        "format": "yyyy-MM-dd HH:mm:ss"
                    },
                    "Datum von": {
                        "type": "keyword"
                    },
                    "Datum bis": {
                        "type": "keyword"
                    },
                    "fct_topic": {
                        "type": "text",
                        "fields": {
                            "keyword": {
                                "type": "keyword",
                                "normalizer": "useLowercase"
                            }
                        }
                    },
                    "gnd_translation": {
                        "type": "text",
                        "fields": {
                            "keyword": {
                                "type": "keyword",
                                "normalizer": "useLowercase"
                            }
                        }
                    },
                    "fct_location": {
                        "type": "text",
                        "fields": {
                            "keyword": {
                                "type": "keyword",
                                "normalizer": "useLowercase"
                            }
                        }
                    },
                    "fct_person_organisation": {
                        "type": "text",
                        "fields": {
                            "keyword": {
                                "type": "keyword",
                                "normalizer": "useLowercase"
                            }
                        }
                    },
                    "gnd_topic": {
                        "properties": {
                            "label": {
                                "type": "text",
                                "fields": {
                                    "keyword": {
                                        "type": "keyword",
                                        "normalizer": "useLowercase"
                                    }
                                }
                            }
                        }
                    },
                }
            }
        }
        ingest_africa_args = {"es": Elasticsearch(self.es_host, timeout=200), "index_name": self.index,
                              "sub_index": self.sub_index, "index_prefix": self.index_prefix, "body": body}
        ingest_africa = IngestEs(**ingest_africa_args)
        ingest_africa.load_data(africa_data)
        ingest_africa.alias_new_index()
