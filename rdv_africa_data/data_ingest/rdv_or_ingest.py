from rdv_utilities import OpenRefine, OpenRefineES


class RDVORIngest(RDVDataIngest):

    def __init__(self, dataset: "RDVDataset"):
        super(RDVORIngest, self).__init__(dataset)

    def ingest_data(self, process_step: str = "final") -> None:

        ingest_settings = self._dataset.dataprocessor.prepare_data4ingest(process_step=process_step, ingest_object=self)
        if ingest_settings:
            or_name, or_headers, or_ingest_data = ingest_settings
            return self.ingest_data_or(name=or_name, headers=or_headers, values=or_ingest_data)
        else:
            or_name, or_headers, or_ingest_data = self._dataset.dataprocessor.prepare_data4ingest(process_step="final", ingest_object=self)
            self.ingest_data_or(name=or_name, headers=or_headers, values=or_ingest_data)


class RDV_ORIngest:

    def _process_data_via_or(self, or_proj_name: str, process_step: str) -> Dict:
        """create new OR Project and apply operations from old OR Project and return data"""

        old_or_project = OpenRefine.findORefProjByName(or_proj_name)
        or_ingest = RDVORIngest(dataset=self.dataset)
        new_or_project = or_ingest.ingest_data(process_step=process_step)

        if old_or_project:
            old_operations = old_or_project.getScriptableProjOperations4Or()
            old_or_project.renameProj(or_proj_name + "_" + str(datetime.datetime.now().timestamp()))
            if old_operations:
                new_or_project.applyWorkflows(old_operations)

                RDV_LOGGER.info("OpenRefine Operations applied: " + "; ".join([operation["description"] for operation in json.loads(old_operations)]))
        return OpenRefineES.getJsonData(or_proj_name)
