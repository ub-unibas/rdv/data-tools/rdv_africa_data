from rdv_marc_ubit import ALMA_MARC_DUMP, AlmaIZMarcJSONRecord, MarcTransformRule
from cache_decorator_redis_ubit import CacheDecorator, NoCacheDecorator
from rdv_africa_data.data_process import RDVCHO
from digi_oai_eportal_ubit.digi_oai_harvester import UBS_SLSP_OAI
from rdv_data_helpers_ubit.projects.oai_harvester.oai_harvester import OAI_ID

from rdv_data_helpers_ubit import IZID_ESFIELD
from xml.etree import ElementTree


class RDVCHO_BSIZ(RDVCHO):
    def __init__(self, dataloader_obj, cho_data, mapping_obj, enricher_obj):
        super().__init__(dataloader_obj, cho_data, mapping_obj, enricher_obj)

    def get_object_id(self) -> str:
        # needs to be string
        object_id = self.cho_data.get("bsiz_id")
        return object_id


class UBSAfricaOAI(UBS_SLSP_OAI):
    index = "ubs_africa"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.marc_rule = kwargs["marc_rule"]
        self.incremental = kwargs["incremental"]
        self.reharvest = kwargs["reharvest"]
        self.last_harvest_date = kwargs["last_harvest_date"]

    def get_data(self):

        return self.harvest_and_process(incremental=self.incremental, reharvest=self.reharvest,
                                        xml_process_func=self.process_africaportal_record,
                                        last_harvest_date=self.last_harvest_date)

    def process_africaportal_record(self, root, **kwargs):
        # 9972500611605504
        # oai:alma.41SLSP_UBS:9929854500105504
        # umwandelt die marc daten nach die elastic struktur via das mapping spreadsheet
        bsiz_id = kwargs.get(OAI_ID).split(":")[-1]
        record = {"type": ["africa_bsiz"], "proj_id": "africa_bsiz", "providing_institution": ["Alma UBS"]}
        oai_header = root.find(".//{http://www.openarchives.org/OAI/2.0/}header")
        try:
            if oai_header and oai_header.attrib.get("status") == "deleted":
                return None, None
            if self.marc_access == "oai":
                xml_data = ElementTree.tostring(root).decode("utf-8")
                al = AlmaIZMarcJSONRecord.get_rec_from_xml(xml_data)
            else:
                al = AlmaIZMarcJSONRecord.get_sysid(bsiz_id, cachedecorator=self.cachedecorator)
            if al:
                bsiz_id = al.get_primary_id()

                # standard Werte
                record.update(al.get_main_info())

                # Here we transfrom the raw marc record filtering only the fields needed
                # according to the "librarian" table (marc_process_spreadsheet in config)
                record.update(self.marc_rule.transform_marc_record(marc_record=al))
                record["alma_links"] = al.get_portfolio_856_links()
            if IZID_ESFIELD not in record:
                return None, record
            else:
                return record[IZID_ESFIELD], record
        except Exception:
            import traceback
            traceback.print_exc()
            self.logger.critical("Problem processing {}".format(bsiz_id))
            return None, None


if __name__ == "__main__":
    alma_dump = ""
    africa_alma_spreadsheet = ""
    gd_service_file = ""
