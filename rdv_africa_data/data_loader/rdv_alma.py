import re
from values_normalizer_ubit import GeoData, UnknownCountryException, GeoObject
from rdv_marc_ubit import ALMA_MARC_DUMP, AlmaIZMarcJSONRecord, MarcTransformRule
from cache_decorator_redis_ubit import CacheDecorator, NoCacheDecorator
from rdv_africa_data.data_process import RDVCHO
from digi_oai_eportal_ubit.digi_oai_harvester import UBS_SLSP_OAI
from rdv_data_helpers_ubit.projects.oai_harvester.oai_harvester import OAI_ID

from rdv_data_helpers_ubit import IZID_ESFIELD
from xml.etree import ElementTree


class RDVCHO_BSIZ(RDVCHO):
    def __init__(self, dataloader_obj, cho_data, mapping_obj, enricher_obj):
        super().__init__(dataloader_obj, cho_data, mapping_obj, enricher_obj)

    def get_object_id(self) -> str:
        # needs to be string
        object_id = self.cho_data.get("bsiz_id")
        return object_id

    def postprocess_data(self, data):
        if not GeoData.initialized:
            GeoData.initialize()
        if "hidden_geoObject" in data:
            # we check if there is a country in the field and add it to fct_countryname
            data["fct_countryname"] = []
            for place in data["hidden_geoObject"]:
                if place in GeoData.country_dict:
                    data["fct_countryname"].append(place)
                else:
                    # for LCSH locations have the following form Mogadishu (Somalia)
                    # therefore we try to extract country from within parenthesis
                    possible_country = self.extract_lcsh_country(place)
                    if possible_country in GeoData.country_dict:
                        data["fct_countryname"].append(possible_country)
                        city = place.split("(")[0].strip()
                        data["hidden_geoObject"].append(city)
            if not data["fct_countryname"]:  # empty array
                data.pop("fct_countryname")  # we remove the key
            else:
                # remove duplicate keys
                data["fct_countryname"] = [*set(data["fct_countryname"])]
        # then we do standard enrichment
        return super().postprocess_data(data)

    @staticmethod
    def extract_lcsh_country(text):
        """
        Given a text, extract the country enclosed in parentheses at the end of the string.
        For LCSH, locations have the following form Mogadishu (Somalia)
        """
        pattern = r"\((.*?)\)$"  # pattern to match text in parentheses at the end of a string
        match = re.search(pattern, text)  # search for the pattern in the text
        if match:
            return match.group(1)  # return the text inside parentheses
        else:
            return None  # if no match is found, return None


class UBSAfricaOAI(UBS_SLSP_OAI):
    index = "ubs_africa"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.marc_rule = kwargs["marc_rule"]
        self.incremental = kwargs["incremental"]
        self.reharvest = kwargs["reharvest"]
        self.last_harvest_date = kwargs["last_harvest_date"]

    def get_data(self):

        return self.harvest_and_process(incremental=self.incremental, reharvest=self.reharvest,
                                        xml_process_func=self.process_africaportal_record,
                                        last_harvest_date=self.last_harvest_date)

    def process_africaportal_record(self, root, **kwargs):
        # 9972500611605504
        # oai:alma.41SLSP_UBS:9929854500105504
        # umwandelt die marc daten nach die elastic struktur via das mapping spreadsheet
        bsiz_id = kwargs.get(OAI_ID).split(":")[-1]
        record = {"type": ["africa_bsiz"], "proj_id": "africa_bsiz", "providing_institution": ["swisscovery Basel"]}
        oai_header = root.find(".//{http://www.openarchives.org/OAI/2.0/}header")
        try:
            if oai_header and oai_header.attrib.get("status") == "deleted":
                return None, None
            if self.marc_access == "oai":
                xml_data = ElementTree.tostring(root).decode("utf-8")
                al = AlmaIZMarcJSONRecord.get_rec_from_xml(xml_data)
            else:
                al = AlmaIZMarcJSONRecord.get_sysid(bsiz_id, cachedecorator=self.cachedecorator)
            if al:
                bsiz_id = al.get_primary_id()

                # standard Werte
                record.update(al.get_main_info())

                # Here we transfrom the raw marc record filtering only the fields needed
                # according to the "librarian" table (marc_process_spreadsheet in config)
                record.update(self.marc_rule.transform_marc_record(marc_record=al))
                # by defaut title has a structure with id and label, we keep the label only for https://ub-basel.atlassian.net/browse/AP-221
                record["title"] = record["title"][0]["label"]

                record["alma_links"] = al.get_portfolio_856_links()
                record["gnd_subject_identifiers"] = al.get_gnd_subject_identifiers()
            if IZID_ESFIELD not in record:
                return None, record
            else:
                return record[IZID_ESFIELD], record
        except Exception:
            import traceback
            traceback.print_exc()
            self.logger.critical("Problem processing {}".format(bsiz_id))
            return None, None


if __name__ == "__main__":
    alma_dump = ""
    africa_alma_spreadsheet = ""
    gd_service_file = ""
