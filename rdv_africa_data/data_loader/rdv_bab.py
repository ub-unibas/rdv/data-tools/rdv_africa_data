import re
import os
from rdv_africa_data.data_loader import utility
from copy import deepcopy
from csv import DictReader
from xml.etree import ElementTree

from values_normalizer_ubit import GeoObject, GeoData

from rdv_africa_data.data_process import RDVCHO

import requests


class RDVCHO_BAB(RDVCHO):
    white_listed_gdoc_functions = set(["build_iiif_link", "clean_coordinates", "extract_country_from_thesaurus"])

    def get_object_id(self) -> str:
        # needs to be string
        object_id = self.cho_data.get("UUID")
        return object_id

    def preprocess_data(self, data):
        preprocessed_data = self.split_bab_values(data)
        return preprocessed_data

    def replace_thesaurus_uuid_by_preferred_term(self, value):
        if value in self.dl_o.thesaurus_uuid_dict:
            pref_term = self.dl_o.thesaurus_id_dict[self.dl_o.thesaurus_uuid_dict[value]]["prefTerm"]
            return pref_term
        else:
            return value

    def replace_thesaurus_uuid_by_notation(self, value):
        if value in self.dl_o.thesaurus_uuid_dict:
            notation = self.dl_o.thesaurus_uuid_dict[value]
            # needs a "." at the end to be able to make prefix queries in the classification
            # todo take thesaurus index name from the config
            # should match the one used for indexing the thesaurus ?
            return "babthesaurus2022_" + notation + "."
        else:
            return value

    # todo use a better name for this (as we replace uuid by preferred term as well)
    # preprocessed_data is one record only
    def split_bab_values(self, preprocessed_data):
        preprocessed_item = {}
        hierarchy_filters = []
        for key, value in preprocessed_data.items():
            if isinstance(value, list):
                preprocessed_item[key] = value

            # only if the field is non-empty
            elif key in self.dl_o.semikolon_fields and value:
                preprocessed_item[key] = [x.strip() for x in value.split(";")]

            elif key in self.dl_o.double_semikolon_fields and value:
                preprocessed_item[key] = [x.strip() for x in value.split(";;")]

            elif key in self.dl_o.multilingual_fields and value:
                multilingual_field = [x.strip() for x in value.split("<\\n>")]
                preprocessed_item[key] = [x for x in multilingual_field if len(x) > 0]

            # only if the field is non-empty
            elif key in self.dl_o.thesaurus_fields and value:
                # if for any reason, the UUID are separated by , instead of ; we replace the , by ;
                value = value.replace(",", ";")
                uuid_array = [x.strip() for x in value.split(";")]
                result = [self.replace_thesaurus_uuid_by_preferred_term(uuid) for uuid in uuid_array]
                notations = [self.replace_thesaurus_uuid_by_notation(uuid) for uuid in uuid_array]
                preprocessed_item[key] = result
                hierarchy_filters += notations
                preprocessed_item["hierarchy_filter"] = hierarchy_filters
            else:
                preprocessed_item[key] = value.strip()

            # process images
            if key == "Digitalisierte Bilder" and value in ["D01_0001", "D01_0002", "D01_0003", "D01_0004"]:
                value = value
                self.logger.info("Uploading image" + value)
                url = "https://ub-mediasrv22.ub.p.unibas.ch:8080/api/items"

                headers = {
                    'Accept': "application/json",
                    'Content-Type': "application/json",
                }

                payload = f"""
                {{
                   "signature": "{value}-5",
                   "source": "https://gitlab.com/ub-unibas/rdv/data-tools/rdv_africa_data/-/raw/main/rdv_africa_data/data/BAB_Archive/Digitalisate/{value}.jpg",
                   "collectionname": "test",
                   "async": false
                }}
                """

                response = requests.request("POST", url, data=payload, headers=headers, verify=False)

                print(response.text)

                preprocessed_item["image_link"] = "https://ub-mediasrv22.ub.p.unibas.ch:8080/test/" + value + "-5/item"

        preprocessed_item = self._set_title(preprocessed_item, self.dl_o.title_field_priority)

        return preprocessed_item

    def _set_title(self, record, title_field_priority):
        """
        Set the title based on a list of potential fields for the title
        :param record: the fields of a record
        :param title_field_priority: a list of field to use as the title field, the first one is the default, and if it is empty, we should use the 2nd and so on
        :return: the updated record
        """
        default_title_field = title_field_priority[0]
        for field in title_field_priority:
            if field in record and len(record[field]) > 0:
                record[default_title_field] = record[field]
                break

        if default_title_field not in record or len(record[default_title_field]) == 0:
            self.logger.warning("Impossible to create a title as all title fields are empty")

        return record

    def build_iiif_link(self, value, es_field, orig_field):
        # use signatur and file where all images are stored and check if signatur is included, if yes add all those images
        paths = self.dl_o.image_lookup.get(value, [])
        try:
            del self.dl_o.image_lookup[value]
        except KeyError:
            pass
        image_ids = []
        for path in paths:
            image_ids.append(path.replace("/", "-"))
        if image_ids:
            print(image_ids)
        return image_ids

    def clean_coordinates(self, value, es_field, orig_field):
        """transform bab coords to es coords"""
        re_wgs84 = re.compile("(?P<NWES>\\w) (?P<grad>\\d+)°((?P<minuten>\\d+)')?")
        coord_rects = []
        coord_values = value.split(";")

        for coord_value in coord_values:
            coord_rect = []
            coordinate_pair = coord_value.split("/")

            for coordinates, coord_type in zip(coordinate_pair, ["lon", "lat"]):
                for number, coordinate in enumerate(coordinates.split("--")):
                    if re_wgs84.search(coordinate):
                        coord_value = self.calc_decimal(re_wgs84.search(coordinate).groupdict())
                        if len(coord_rect) < 2:
                            coord_rect.append([coord_value])
                        else:
                            coord_rect[number].append(coord_value)
            # check if 4 values
            for lines in coord_rect:
                if len(lines) != 2 or len(coord_rect) != 2:
                    self.dl_o.logger.info("falsche Koordinatenangaben:" + value)
                    break
            else:
                if coord_rect:
                    geo_env = {"type": "envelope", "coordinates": coord_rect}
                    coord_rects.append(geo_env)

        if coord_rects:
            return coord_rects

    # This can be called directly from the mapping table via
    # $extract_country_from_thesaurus
    def extract_country_from_thesaurus(self, value, es_field, orig_field):
        thesaurus = self.dl_o.thesaurus_uuid_dict
        if thesaurus.get(value, ""):
            # if under 1.1 in thesaurus
            if self.is_place(value) and 0:
                place, country = self.extract_geoinfo(self.get_broader_terms(value))
                geographikum = GeoObject(place=place, country=country,
                                         sub_enrich_field=orig_field, log_data=self._log_data)
                return geographikum
        elif value and value not in thesaurus:
            self.dl_o.logger.warning("kein Thesaruseintrag: " + value + " " + str(value))

    @staticmethod
    def calc_decimal(coord_dict):
        value = ""
        for key in ["grad", "minuten"]:
            if key == "grad":
                value = coord_dict[key]
            elif key == "minuten" and "minuten" in coord_dict and coord_dict["minuten"]:
                value = value + "." + str(int(int(coord_dict[key]) * 100 / 60))
        if coord_dict["NWES"] in ["S", "W"]:
            value = "-" + value
        return float(value)

    def is_place(self, value):
        if value in self.dl_o.thesaurus_uuid_dict and "1.1" in \
                self.dl_o.thesaurus_id_dict[self.dl_o.thesaurus_uuid_dict[value]]["ancestors"]:
            return 1

    def get_broader_terms(self, value):
        ancestors_names = []
        for key in self.dl_o.thesaurus_id_dict[self.dl_o.thesaurus_uuid_dict[value]]["ancestors"]:
            if key:
                ancestors_names.append(self.dl_o.thesaurus_id_dict[key]["prefTerm"])
        ancestors_names.append(value)
        return ancestors_names

    def extract_geoinfo(self, values):
        place, country = "", ""
        for value in values:
            if 0 and value in GeoData.country_dict:
                country = value
        # if last value is not the same as country value set place value
        if country != values[-1]:
            place = values[-1]
        return place, country


class RDVDataBABLibrary():

    def __init__(self, logger, **kwargs):
        self.logger = logger

        # the key is the notation (for example 1.1.2) and the value is the full thesaurus entry
        self.thesaurus_id_dict = {}

        # the key is the uuid and the value is the notation (for example 1.1.2)
        self.thesaurus_uuid_dict = {}

        self.folder = kwargs["folder"]
        self.build_thesaurus(kwargs["thesaurus_filename"])
        # self.image_lookup = self.get_image_lookup(kwargs["img_lookup"])
        self.thesaurus_fields = ["Form", "Inhalt", "Region"]
        self.semikolon_fields = ["Verfasser/in", "Ort", "Verlag", "Reihe/Serie", "Herausgeber/in", "Erhalten von",
                                 "Regie/Produzent", "Schauspieler", "Sonst. Personen", "Interpret/in",
                                 "Komponist/in", "Verlag/Studio", "Sprache", "Länge"]
        self.double_semikolon_fields = []

        # the field to use for the title, if the first field in the list is empty take the second, etc.
        # e.g. if Titel is empty, use Reihe/Serie
        self.title_field_priority = ["Titel", "Reihe/Serie"]

        # These field have a <\n> to separate the various languages
        self.multilingual_fields = []

    # We load all the csv files which are in the folder
    def get_data(self):
        # csv_data_all_files = utility.merge_multiple_csv_files(self.folder)
        # csv_data_all_files = self.get_csv_data("/home/lionel/Documents/mycloud/arbim/git_repo/afrika-portal/rdv_africa_data/rdv_africa_data/data/BAB_Library/AufsätzeBücher22.1.csv", encoding="ISO-8859-1")
        # csv_data_all_files = self.get_csv_data("/home/lionel/Documents/mycloud/arbim/git_repo/afrika-portal/rdv_africa_data/tests/result.csv")

        csv_files = [f for f in os.listdir(self.folder) if f.endswith(".csv")]
        csv_data_all_files = []
        for csv_file in csv_files:
            print("Processing " + csv_file)
            csv_data = self.get_csv_data(self.folder + "/" + csv_file, encoding="ISO-8859-1")
            csv_data_all_files.extend(csv_data)

        for record in csv_data_all_files:
            record["type"] = ["bablibrary2022"]
            record["proj_id"] = "bablibrary2022"
            record["providing_institution"] = ["BABLibrary2022"]
        return csv_data_all_files

    def get_image_lookup(self, img_lookup_file):
        image_lookup = {}
        with open(img_lookup_file) as image_fh:

            for line in image_fh:
                line = line.strip("./\n")
                for type_ in [".jpg", ".TIF", ".tif"]:
                    if line.endswith(type_) and line.startswith("bab/Druckvariante"):
                        filename = line.split("/")[-1].split(type_)[0]
                        id_parts = re.split("[_-]", filename)
                        id_ = id_parts[0].lstrip("0")
                        if len(id_parts) > 1 and id_parts[1].isnumeric():
                            id_ = "{}: {}".format(id_, id_parts[1])
                        if not id_.startswith("EX"):
                            id_ = "X {}".format(id_)
                        image_lookup.setdefault(id_, []).append(line)
        return image_lookup

    def get_csv_data(self, filepath, encoding: str = "utf-8") -> dict:
        data = []
        with open(filepath, encoding=encoding) as file_fh:
            for line in DictReader(file_fh, delimiter=";"):
                data.append(line)
        return data

    def build_thesaurus(self, thesaurus_file):

        thesaurus_xml = ElementTree.parse(thesaurus_file)
        thesaurus_root = thesaurus_xml.getroot()

        # todo: findall
        for concept in thesaurus_root:
            notation = concept.findtext("notation")
            identifier = concept.findtext("creation/identifier")
            pref_term = concept.findtext("prefTerm")
            broader = [concept.findtext("broader")]
            narrower = [item.text for item in concept.iter("narrower")]
            thesaurus_entry = {"identifier": identifier, "notation": notation,
                               "prefTerm": pref_term, "parents": broader, "children": narrower, "ancestors": []}
            self.thesaurus_id_dict.setdefault(notation, thesaurus_entry)
            if identifier not in self.thesaurus_uuid_dict:
                self.thesaurus_uuid_dict[identifier] = notation
            else:
                print("double uuid", identifier)

        self.build_thesarus_recursive("1", "", [])

    def build_thesarus_recursive(self, key, parent, former_parents):
        parents = deepcopy(former_parents)
        parents.append(parent)
        if key in self.thesaurus_id_dict:
            self.thesaurus_id_dict[key]["ancestors"] = parents
        else:
            print(key + " has no ancestors field")

        for child in self.thesaurus_id_dict[key]["children"]:
            self.build_thesarus_recursive(child, key, parents)


class RDVDataBABArchive(RDVDataBABLibrary):

    def __init__(self, logger, **kwargs):

        self.logger = logger

        # the key is the notation (for example 1.1.2) and the value is the full thesaurus entry
        self.thesaurus_id_dict = {}

        # the key is the uuid and the value is the notation (for example 1.1.2)
        self.thesaurus_uuid_dict = {}

        self.folder = kwargs["folder"]
        self.build_thesaurus(kwargs["thesaurus_filename"])

        # self.image_lookup = self.get_image_lookup(kwargs["img_lookup"])
        self.thesaurus_fields = ["Deskriptoren", "Regionen / Länder", "Genres"]
        self.semikolon_fields = ["Interpreten", "Gesprächspartner", "Komponist", "Personen", "Körperschaften",
                                 "Fotografen", "Übersicht Bild", "Übersicht Ton", "Übersicht Film",
                                 "Übersicht Schrift", "Fotograf/-in", "Regie / Produzent", "Verfasser",
                                 "Herausgeber", "Umfang Bild"]
        self.double_semikolon_fields = ["Darin"]
        # These field have a <\n> to separate the various languages
        self.multilingual_fields = ["Kurzbiographien", "Bemerkungen", "Bestandsbeschreibung"]

        # the field to use for the title, if the first field in the list is empty take the second, etc.
        # e.g. if Titel is empty, use Signatur as title
        self.title_field_priority = ["Titel", "Signatur", "Bestandskürzel"]

    # We load all the csv files which are in the folder
    # todo : use the same get_data as for BAB Library and remove this
    def get_data(self):
        # csv_data_all_files = utility.merge_multiple_csv_files(self.folder)
        # csv_data_all_files = self.get_csv_data("/home/lionel/Documents/mycloud/arbim/git_repo/afrika-portal/rdv_africa_data/rdv_africa_data/data/BAB_Library/AufsätzeBücher22.1.csv", encoding="ISO-8859-1")
        # csv_data_all_files = self.get_csv_data("/home/lionel/Documents/mycloud/arbim/git_repo/afrika-portal/rdv_africa_data/tests/result.csv")

        csv_files = [f for f in os.listdir(self.folder) if f.endswith(".csv")]
        csv_data_all_files = []
        for csv_file in csv_files:
            print("Processing " + csv_file)
            csv_data = self.get_csv_data(self.folder + "/" + csv_file, encoding="ISO-8859-1")
            csv_data_all_files.extend(csv_data)

        for record in csv_data_all_files:
            record["type"] = ["babarchive2022"]
            record["proj_id"] = "babarchive2022"
            record["providing_institution"] = ["BABArchive2022"]
        return csv_data_all_files


if __name__ == "__main__":
    img_lookup = "/home/martin/PycharmProjects/rdv_africaslsp_data/rdv_africa_data/project_data/bab_library/all_images_ap.txt"
    thesaurus = "/home/martin/PycharmProjects/rdv_africaslsp_data/rdv_africa_data/project_data/bab_library/BABZentralthesaurus.xml"
    file = "/home/martin/PycharmProjects/rdv_africaslsp_data/rdv_africa_data/project_data/bab_library/bab_library/KartenCSV.csv"
    bab_data = RDVDataBABLibrary(file=file, img_lookup=img_lookup, thesaurus_filename=thesaurus)
    print(bab_data.get_data())
