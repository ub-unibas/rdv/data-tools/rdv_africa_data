import re
import os
from urllib.request import urlopen

from config import config
from media_server.media_server_client import MediaServerClient
from rdv_africa_data.data_loader import utility
from copy import deepcopy
from csv import DictReader
from xml.etree import ElementTree

from values_normalizer_ubit import GeoObject, GeoData

from rdv_africa_data.data_process import RDVCHO

from wand.image import Image

import requests


class RDVCHO_BAB(RDVCHO):

    def get_object_id(self) -> str:
        # needs to be string
        object_id = self.cho_data.get("UUID")
        return object_id

    def preprocess_data(self, data):
        preprocessed_data = self.split_bab_values(data)

        if "Objektart" in preprocessed_data and "Signatur" in preprocessed_data and preprocessed_data["Objektart"] in ["Alben", "Einzelbilder"]:
            if preprocessed_data["Signatur"] in self.dl_o.signatures_with_disclaimer:
                preprocessed_data["Disclaimer"] = "In this folder, one or more images have been edited to allow online access despite sensitive content. If you wish to have access to the unedited version of this image, kindly contact us. For more information, please see our Ethical Guidelines."
        if "Bestand" in preprocessed_data:
            preprocessed_data["Bestand"] = preprocessed_data["Bestand"].replace("_", "")

        return preprocessed_data

    def postprocess_data(self, data):
        # data is a single record
        data = super().postprocess_data(data)

        # add image info
        # we use the "Datei" field which has the name of the file
        media_server_collection = self.dl_o.media_server_collection

        # for BAB Library, only RARA and Plakat and ePlakat
        # cf. https://ub-basel.atlassian.net/wiki/spaces/AF/pages/741114525/BAB+Bin+rdaten
        if "Objektart" in data and "Signatur" in data and data["Objektart"] in ["E-Plakat-Sammlung", "Plakat-Sammlung", "Rara-Projekt"]:
            signature = data["Signatur"].lower()
            if signature in self.dl_o.images:
                data = self.add_one_image(data, media_server_collection, signature, 0)
            # Signature "X 750: 1" ist für Bild "X 750_1"
            elif signature.replace(": ", "_") in self.dl_o.images:
                data = self.add_one_image(data, media_server_collection, signature.replace(": ", "_"), 0)
            # Rückseite
            if signature + "_r" in self.dl_o.images:
                data = self.add_one_image(data, media_server_collection, signature + "_r", 1)
            if signature + "_1" in self.dl_o.images:
                data = self.add_one_image(data, media_server_collection, signature + "_1", 0)
            if signature + "_2" in self.dl_o.images:
                data = self.add_one_image(data, media_server_collection, signature + "_2", 1)

        # for BAB Archive, Alben und Einzelbilder
        if "Objektart" in data and "Signatur" in data and data["Objektart"] in ["Alben", "Einzelbilder"]:
            signature = data["Signatur"].lower()
            if signature.startswith("s0") or signature.startswith("s1"):  # Zuordnungsschlüssel 3 (cf. https://ub-basel.atlassian.net/browse/AP-136)
                signature_image = signature.replace(".", "_").replace("s00", "s0").replace("s0", "s").replace(" (n)", "").replace(" (p)", "")
            else:  # Zuordnungsschlüssel 1, 2 und 4
                signature_image = signature.replace(".", "_").replace(" ", "_")

            relevant_images = self.extract_elements(self.dl_o.images, signature_image)

            counter = 0
            # sort images by page number
            relevant_images = sorted(relevant_images, key=self.extract_page_order)

            for key in relevant_images:
                data = self.add_one_image(data, media_server_collection, key, counter)
                counter = counter + 1

        return data

    @staticmethod
    def extract_page_order(text):
        """
        from page001 extract 1
        for the cover pages (cover1) assign a lower index
        for the rear pages (rear1) assign a higher index
        page: a string like page001 or K02_page13
        :return:
        """
        offset = 0
        # Assign a higher value to "rear" pages and a lower value to "cover" pages
        if "rear" in text:
            offset = 10000
        elif "cover" in text:
            offset = -10000

        # Extract the numeric part from the page number
        matches = re.findall(r'\d+', text)
        if matches:
            return offset + int(matches[-1])
        else:
            return 0

    @staticmethod
    def extract_elements(dictionary, prefix):
        """
        Return a dictionary with all the elements where the key starts with prefix
        :param dictionary:
        :param prefix:
        :return:
        """
        return {key: value for key, value in dictionary.items() if key.startswith(prefix)}

    def add_one_image(self, data, media_server_collection, signature, position):
        """
        Add one image to the manifest
        :param data: the record
        :param media_server_collection:
        :param signature:
        :return:
        """

        image = self.dl_o.images[signature]
        iiif_img = {}
        iiif_img["width"] = image["width"]
        iiif_img["height"] = image["height"]
        iiif_img["id"] = image["signature"]
        iiif_img["pos"] = position
        if "iiif_imgs" in data:
            data["iiif_imgs"].append(iiif_img)
        else:
            data["iiif_imgs"] = [iiif_img]
        # todo take this somewhere else at the end
        data["media_server_collection"] = media_server_collection
        data["hidden_digitized"] = "ja"

        return data

    def replace_thesaurus_uuid_by_preferred_term(self, value):
        if value in self.dl_o.thesaurus_uuid_dict:
            pref_term = self.dl_o.thesaurus_id_dict[self.dl_o.thesaurus_uuid_dict[value]]["prefTerm"]
            return pref_term
        else:
            return value

    def replace_thesaurus_uuid_by_notation(self, value):
        if value in self.dl_o.thesaurus_uuid_dict:
            notation = self.dl_o.thesaurus_uuid_dict[value]
            # needs a "." at the end to be able to make prefix queries in the classification
            # todo take thesaurus index name from the config
            # should match the one used for indexing the thesaurus ?
            return notation
        else:
            return value

    # todo use a better name for this (as we replace uuid by preferred term as well)
    # preprocessed_data is one record only
    def split_bab_values(self, preprocessed_data):
        preprocessed_item = {}
        hierarchy_filters = []
        for key, value in preprocessed_data.items():
            if isinstance(value, list):
                preprocessed_item[key] = value

            # only if the field is non-empty
            elif key in self.dl_o.semikolon_fields and value:
                preprocessed_item[key] = [x.strip() for x in value.split(";")]

            elif key in self.dl_o.double_semikolon_fields and value:
                preprocessed_item[key] = [x.strip() for x in value.split(";;")]

            elif key in self.dl_o.multilingual_fields and value:
                multilingual_field = [x.strip() for x in value.split("<\\n>")]
                preprocessed_item[key] = [x for x in multilingual_field if len(x) > 0]

            # only if the field is non-empty
            elif key in self.dl_o.thesaurus_fields and value:
                # if for any reason, the UUID are separated by , instead of ; we replace the , by ;
                value = value.replace(",", ";")
                uuid_array = [x.strip() for x in value.split(";")]
                result = [self.replace_thesaurus_uuid_by_preferred_term(uuid) for uuid in uuid_array]
                notations = [self.replace_thesaurus_uuid_by_notation(uuid) for uuid in uuid_array]
                preprocessed_item[key] = result
                hierarchy_filters += ["babthesaurus2022_" + notation + "." for notation in notations]
                preprocessed_item["hierarchy_filter"] = hierarchy_filters
                if key == "Region" or key == "Regionen / Länder":
                    preprocessed_item["fct_countryname"] = self.extract_country_from_notations(notations)
                    # only if the field is non-empty
            elif key == "Klassifikation" and value:
                value = value.replace(",", ";")
                klassifikation_uuid_array = [x.strip() for x in value.split(";")]
                result = [self.dl_o.thesaurus_klassifikation_uuid_dict.get(uuid, uuid) for uuid in klassifikation_uuid_array]
                preprocessed_item[key] = result
            else:
                preprocessed_item[key] = value.strip()

        preprocessed_item = self._set_title(preprocessed_item, self.dl_o.title_field_priority)

        return preprocessed_item

    def _set_title(self, record, title_field_priority):
        """
        Set the title based on a list of potential fields for the title
        :param record: the fields of a record
        :param title_field_priority: a list of field to use as the title field, the first one is the default, and if it is empty, we should use the 2nd and so on
        :return: the updated record
        """
        default_title_field = title_field_priority[0]
        for field in title_field_priority:
            if field in record and len(record[field]) > 0:
                record[default_title_field] = record[field]
                break

        if default_title_field not in record or len(record[default_title_field]) == 0:
            self.logger.warning("Impossible to create a title as all title fields are empty")

        return record

    def extract_country_from_notations(self, notations):
        """
        The BAB thesaurus for geographical places (starting with 1.1) is structured as follows :  Kontinent, Teil, Land, Region, Stadt)
        Therefore if Brandberg is 1.1.1.1.2.9.1. the country is 1.1.1.1.2.9 (always the 6th level)
        :param notations: an array of thesaurus notations like 1.1.3.3.
        :return: the country names
        """
        countries = []
        for notation in notations:
            is_place = self.is_place(notation)
            if is_place:
                country = self.extract_country(self.get_broader_terms(notation))
                if country:
                    countries.append(country)
        return countries

    @staticmethod
    def is_place(value):
        if value.startswith("1.1"):
            return 1

    def get_broader_terms(self, value):
        ancestors_names = []
        for key in self.dl_o.thesaurus_id_dict[value]["ancestors"]:
            if key:
                ancestors_names.append(self.dl_o.thesaurus_id_dict[key]["prefTerm"])
        ancestors_names.append(self.dl_o.thesaurus_id_dict[value]["prefTerm"])
        return ancestors_names

    def extract_country(self, values):
        """
        Extract the first country from a list of notations (broader terms)
        :param values:
        :return:
        """
        for value in values:
            if value in GeoData.country_dict:
                return value


class RDVDataBABLibrary():

    def __init__(self, logger, **kwargs):
        self.logger = logger

        # the key is the notation (for example 1.1.2) and the value is the full thesaurus entry
        self.thesaurus_id_dict = {}

        # the key is the uuid and the value is the notation (for example 1.1.2)
        self.thesaurus_uuid_dict = {}

        self.folder = kwargs["folder"]
        self.build_thesaurus(kwargs["thesaurus_filename"])
        self.thesaurus_fields = ["Form", "Inhalt", "Region"]
        self.semikolon_fields = ["Verfasser/in", "Ort", "Verlag", "Reihe/Serie", "Herausgeber/in", "Erhalten von",
                                 "Regie/Produzent", "Schauspieler", "Sonst. Personen", "Interpret/in",
                                 "Komponist/in", "Verlag/Studio", "Sprache", "Länge"]
        self.double_semikolon_fields = []

        # the field to use for the title, if the first field in the list is empty take the second, etc.
        # e.g. if Titel is empty, use Reihe/Serie
        self.title_field_priority = ["Titel", "Reihe/Serie"]

        # These field have a <\n> to separate the various languages
        self.multilingual_fields = []

        self.media_server_collection = kwargs["media_server_collection"]
        self.images = self.get_list_of_available_images(self.media_server_collection)

    # We load all the csv files which are in the folder
    def get_data(self):
        # csv_data_all_files = utility.merge_multiple_csv_files(self.folder)
        # csv_data_all_files = self.get_csv_data("/home/lionel/Documents/mycloud/arbim/git_repo/afrika-portal/rdv_africa_data/rdv_africa_data/data/BAB_Library/AufsätzeBücher22.1.csv", encoding="ISO-8859-1")
        # csv_data_all_files = self.get_csv_data("/home/lionel/Documents/mycloud/arbim/git_repo/afrika-portal/rdv_africa_data/tests/result.csv")

        csv_files = [f for f in os.listdir(self.folder) if f.endswith(".csv")]
        csv_data_all_files = []
        for csv_file in csv_files:
            self.logger.info("Processing " + csv_file)
            csv_data = self.get_csv_data(self.folder + "/" + csv_file, encoding="ISO-8859-1")
            csv_data_all_files.extend(csv_data)

        for record in csv_data_all_files:
            record["type"] = ["bablibrary2022"]
            record["proj_id"] = "bablibrary2022"
            record["providing_institution"] = ["Basler Afrika Bibliographien Bibliothek"]
        return csv_data_all_files

    def get_csv_data(self, filepath, encoding: str = "utf-8") -> dict:
        data = []
        with open(filepath, encoding=encoding) as file_fh:
            for line in DictReader(file_fh, delimiter=";"):
                data.append(line)
        return data

    def get_list_of_available_images(self, collection):
        """
        Get all items from a collection in the media server and return a dictionary of dictionaries containing
        signature, width and height, with the signature as a key

        :param collection:
        :return:
        """
        media_server_client = MediaServerClient()
        items = media_server_client.get_items(collection=collection)

        results = {}
        if items:
            for item in items:
                # todo check if key exists
                if "signature" in item:
                    if "metadata" in item and "width" in item["metadata"] and "height" in item["metadata"]:
                        result = {
                            "signature": item["signature"],
                            "width": item["metadata"]["width"],
                            "height": item["metadata"]["height"]
                        }
                        results[item["signature"]] = result
                    else:
                        self.logger.info("Missing metadata for image " + item["signature"])
                else:
                    self.logger.info("Problem with media server")
        return results

    def build_thesaurus(self, thesaurus_file):

        thesaurus_xml = ElementTree.parse(thesaurus_file)
        thesaurus_root = thesaurus_xml.getroot()

        # todo: findall
        for concept in thesaurus_root:
            notation = concept.findtext("notation")
            identifier = concept.findtext("creation/identifier")
            pref_term = concept.findtext("prefTerm")
            broader = [concept.findtext("broader")]
            narrower = [item.text for item in concept.iter("narrower")]
            thesaurus_entry = {"identifier": identifier, "notation": notation,
                               "prefTerm": pref_term, "parents": broader, "children": narrower, "ancestors": []}
            self.thesaurus_id_dict.setdefault(notation, thesaurus_entry)
            if identifier not in self.thesaurus_uuid_dict:
                self.thesaurus_uuid_dict[identifier] = notation
            else:
                self.logger.info("double uuid", identifier)

        self.build_thesarus_recursive("1", "", [])

    def build_thesarus_recursive(self, key, parent, former_parents):
        parents = deepcopy(former_parents)
        parents.append(parent)
        if key in self.thesaurus_id_dict:
            self.thesaurus_id_dict[key]["ancestors"] = parents
        else:
            self.logger.info(key + " has no ancestors field")

        for child in self.thesaurus_id_dict[key]["children"]:
            self.build_thesarus_recursive(child, key, parents)


class RDVDataBABArchive(RDVDataBABLibrary):

    def __init__(self, logger, **kwargs):

        self.logger = logger

        # the key is the notation (for example 1.1.2) and the value is the full thesaurus entry
        self.thesaurus_id_dict = {}

        # the key is the uuid and the value is the notation (for example 1.1.2)
        self.thesaurus_uuid_dict = {}

        self.thesaurus_klassifikation_id_dict = {}
        self.thesaurus_klassifikation_uuid_dict = {}

        self.folder = kwargs["folder"]
        self.build_thesaurus(kwargs["thesaurus_filename"])
        self.build_thesaurus_klassifikation(kwargs["thesaurus_klassifikation_filename"])

        self.thesaurus_fields = ["Deskriptoren", "Regionen / Länder", "Genres"]
        self.semikolon_fields = ["Interpreten", "Gesprächspartner", "Komponist", "Personen", "Körperschaften",
                                 "Fotografen", "Übersicht Bild", "Übersicht Ton", "Übersicht Film",
                                 "Übersicht Schrift", "Fotograf/-in", "Regie / Produzent", "Verfasser",
                                 "Herausgeber", "Umfang Bild", "Orte", "Sprachen", "Ort der Aufnahme", "Stichwort"]
        self.double_semikolon_fields = ["Darin"]
        # These field have a <\n> to separate the various languages or the various entries
        self.multilingual_fields = ["Kurzbiographien", "Bemerkungen", "Bestandsbeschreibung", "Inhalt"]

        # the field to use for the title, if the first field in the list is empty take the second, etc.
        # e.g. if Titel is empty, use Signatur as title
        self.title_field_priority = ["Titel", "Signatur", "Bestandskürzel"]

        self.media_server_collection = kwargs["media_server_collection"]
        self.images = self.get_list_of_available_images(self.media_server_collection)

        self.signatures_with_disclaimer = [
            "A06",
            "BPA.39 1",
            "BPA.159 1",
            "BPA.159 2",
            "D01 1991",
            "D01 1988",
            "D01 1962",
            "D01 1866",
            "D01 1865",
            "D01 1864",
            "D01 1851",
            "D01 1829",
            "D01 1828",
            "D01 1827",
            "D01 1826",
            "D01 1825",
            "D01 1824",
            "D01 1494",
            "D01 1426",
            "D01 1046",
            "D01 0860",
            "D01 0859",
            "D01 0858",
            "D01 0662",
            "D01 0661",
            "D01 0660",
            "D01 0497",
            "D01 0496",
            "BPA.25 004",
            "BPA.25 006",
            "BPA.25 011",
            "BPA.25 013",
            "BPA.25 018",
            "BPA.25 024",
            "BPA.25 039",
            "BPA.25 041",
            "BPA.25 045",
            "BPA.25 052",
            "BPA.25 054",
            "BPA.25 056",
            "BPA.25 062",
            "BPA.25 075",
            "BPA.25 079",
            "BPA.25 091",
            "BPA.25 095",
            "BPA.25 099",
            "BPA.25 111",
            "BPA.138 012",
            "BPA.138 038",
            "BPA.138 079",
            "BPA.138 108",
            "BPA.165 010"
        ]

    # We load all the csv files which are in the folder
    # todo : use the same get_data as for BAB Library and remove this
    def get_data(self):
        # csv_data_all_files = utility.merge_multiple_csv_files(self.folder)
        # csv_data_all_files = self.get_csv_data("/home/lionel/Documents/mycloud/arbim/git_repo/afrika-portal/rdv_africa_data/rdv_africa_data/data/BAB_Library/AufsätzeBücher22.1.csv", encoding="ISO-8859-1")
        # csv_data_all_files = self.get_csv_data("/home/lionel/Documents/mycloud/arbim/git_repo/afrika-portal/rdv_africa_data/tests/result.csv")

        csv_files = [f for f in os.listdir(self.folder) if f.endswith(".csv")]
        csv_data_all_files = []
        for csv_file in csv_files:
            self.logger.info("Processing " + csv_file)
            csv_data = self.get_csv_data(self.folder + "/" + csv_file, encoding="ISO-8859-1")
            csv_data_all_files.extend(csv_data)

        for record in csv_data_all_files:
            record["type"] = ["babarchive2022"]
            record["proj_id"] = "babarchive2022"
            record["providing_institution"] = ["Basler Afrika Bibliographien Archiv"]
        return csv_data_all_files

    def build_thesaurus_klassifikation(self, thesaurus_file):

        thesaurus_xml = ElementTree.parse(thesaurus_file)
        thesaurus_root = thesaurus_xml.getroot()

        # todo: findall
        for concept in thesaurus_root:
            notation = concept.findtext("notation")
            identifier = concept.findtext("creation/identifier")
            pref_term = concept.findtext("prefTerm")
            broader = [concept.findtext("broader")]
            narrower = [item.text for item in concept.iter("narrower")]
            thesaurus_entry = {"identifier": identifier, "notation": notation,
                               "prefTerm": pref_term, "parents": broader, "children": narrower, "ancestors": []}
            self.thesaurus_klassifikation_id_dict.setdefault(notation, thesaurus_entry)
            if identifier not in self.thesaurus_klassifikation_uuid_dict:
                self.thesaurus_klassifikation_uuid_dict[identifier] = notation
            else:
                self.logger.info("double uuid", identifier)

        self.build_thesarus_klassifikation_recursive("1", "", [])
        # now replace notation by concatenated prefTerm in thesaurus_klassifikation_uuid_dict
        for concept in self.thesaurus_klassifikation_uuid_dict.keys():
            values = []
            notation = self.thesaurus_klassifikation_uuid_dict[concept]
            ancestors = self.thesaurus_klassifikation_id_dict[notation]["ancestors"]
            for ancestor in ancestors:
                if ancestor != "":
                    values.append(self.thesaurus_klassifikation_id_dict[ancestor]["prefTerm"])
            values.append(self.thesaurus_klassifikation_id_dict[notation]["prefTerm"])
            if "<bab>" in values:
                values.remove("<bab>")
            self.thesaurus_klassifikation_uuid_dict[concept] = (" / ").join(values)

    def build_thesarus_klassifikation_recursive(self, key, parent, former_parents):
        parents = deepcopy(former_parents)
        parents.append(parent)
        if key in self.thesaurus_klassifikation_id_dict:
            self.thesaurus_klassifikation_id_dict[key]["ancestors"] = parents
        else:
            self.logger.info(key + " has no ancestors field")

        for child in self.thesaurus_klassifikation_id_dict[key]["children"]:
            self.build_thesarus_klassifikation_recursive(child, key, parents)
