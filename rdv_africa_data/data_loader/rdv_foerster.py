import os
import pathlib
from csv import DictReader
from random import randint

from media_server.media_server_client import MediaServerClient
from rdv_africa_data.data_process import RDVCHO

from config import config


from libxmp.utils import file_to_dict
from libxmp import XMPFiles, consts


class RDVCHOFoerster(RDVCHO):
    def preprocess_data(self, data):
        data = super().preprocess_data(data)
        preprocessed_item = self.extract_foerster_values(data)
        return preprocessed_item

    def postprocess_data(self, data):
        # data is a single record
        data = super().postprocess_data(data)

        # add image info
        # we use the "Datei" field which has the name of the file
        if hasattr(self.dl_o, "media_server_collection"):
            media_server_collection = self.dl_o.media_server_collection
            if "signature" in data:
                signature = data["signature"].lower()
                if signature in self.dl_o.images:
                    image = self.dl_o.images[signature]
                    iiif_img = {}
                    iiif_img["width"] = image["width"]
                    iiif_img["height"] = image["height"]
                    iiif_img["id"] = image["signature"]
                    iiif_img["pos"] = 0
                    data["iiif_imgs"] = [iiif_img]
                    data["media_server_collection"] = media_server_collection
                    data["hidden_digitized"] = "ja"
        return data

    # data is one record only, should name it item or record
    def extract_foerster_values(self, data):
        return data

    def get_object_id(self) -> str:
        # needs to be string
        # object_id = self.cho_data.get("TPH Picture ID")
        # object_id = randint(1, 10000)
        object_id = self.cho_data.get("signature")
        return object_id


class RDVDataFoerster():

    def __init__(self, logger, **kwargs):
        self.logger = logger
        self.folder = kwargs["folder"]
        self.media_server_collection = kwargs["media_server_collection"]
        self.images = self.get_list_of_available_images(self.media_server_collection)

    def get_data(self):
        data = []
        folder_path = pathlib.Path(self.folder)
        for file in list(folder_path.rglob("*")):
            filename = os.path.basename(file)

            if file.is_file() and not (filename.startswith(".")) and (
                    filename.lower().endswith(".tif") or filename.lower().endswith(".jpg") or filename.lower().endswith(
                    ".tiff") or filename.lower().endswith(".dng") or filename.lower().endswith(
                    ".psd") or filename.lower().endswith(".jpeg")):

                try:
                    record = {}
                    record["filepath"] = str(file)
                    record["basename"] = os.path.basename(file)
                    record["dirname"] = os.path.dirname(file)
                    record["signature"] = os.path.splitext(os.path.basename(file))[0]

                    record["type"] = ["foerster"]
                    record["proj_id"] = "foerster"
                    record["providing_institution"] = ["Fotographische Sammlung Förster"]
                    # xmp_file = XMPFiles(file_path=str(file), open_forupdate=True)
                    # xmp = xmp_file.get_xmp()

                    # print(xmp.get_property(consts.XMP_NS_DC, 'format'))
                    # print(xmp.get_property(consts.XMP_NS_DC, 'title[1]'))

                    xmpdict = file_to_dict(str(file))
                    # print(xmpdict)
                    if consts.XMP_NS_DC in xmpdict:
                        dc = xmpdict[consts.XMP_NS_DC]

                        # print("\n\n")
                        # print(file)
                        # print("--------------------------------------------")
                        for property in dc:
                            if (not ("lang" in property[0])):
                                record[property[0].replace(":", "_").replace("[", "_").replace("]", "")] = property[1]
                        record["dc_subject"] = [val for key, val in record.items() if key.startswith("dc_subject") and val]

                    hierarchical_subject_separator = " / "

                    if consts.XMP_NS_Lightroom in xmpdict:
                        lr = xmpdict[consts.XMP_NS_Lightroom]
                        for property in lr:
                            if (not ("lang" in property[0])):
                                record[property[0].replace(":", "_").replace("[", "_").replace("]", "")] = property[1]
                            record["lr_hierarchicalSubject"] = [val.replace("|", hierarchical_subject_separator) for key, val in record.items() if key.startswith("lr_hierarchicalSubject") and val]

                    # extract country and place
                    # in the source they look like this
                    # Africa|Burkina Faso|Ouagadougou
                    # Africa|Cameroon / Cameroun|Bamenda
                    # Africa|Cameroon / Cameroun|Foumban
                    # Africa|Côte d'Ivoire|Abidjan
                    for topic in record.get("lr_hierarchicalSubject", []):
                        topic.replace("Cameroon / Cameroun", "Cameroun")
                        if topic.startswith("Africa"):
                            locations = topic.split(hierarchical_subject_separator)
                            if len(locations) > 0:
                                record["country"] = locations[1]
                            if len(locations) > 1:
                                record["place"] = locations[2]

                    # print(xmp)
                    data.append(record)
                except Exception as e:
                    self.logger.info(e)
                    self.logger.info("Problem with " + filename)

        return data

    def get_list_of_available_images(self, collection):
        """
        Get all items from a collection in the media server and return a dictionary of dictionaries containing
        signature, width and height, with the signature as a key

        :param collection:
        :return:
        """
        media_server_client = MediaServerClient()
        items = media_server_client.get_items(collection=collection)

        results = {}
        for item in items:
            # todo check if key exists
            if "signature" in item:
                if "metadata" in item and "width" in item["metadata"] and "height" in item["metadata"]:
                    result = {
                        "signature": item["signature"],
                        "width": item["metadata"]["width"],
                        "height": item["metadata"]["height"]
                    }
                    results[item["signature"]] = result
                else:
                    self.logger.info("Missing metadata for image " + item["signature"])
            else:
                self.logger.info("Problem with media server")
        return results
