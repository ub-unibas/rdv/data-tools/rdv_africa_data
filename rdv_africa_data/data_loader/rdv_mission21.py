import re
from csv import DictReader

from rdv_africa_data.data_process import RDVCHO


class RDVCHO_Mission21(RDVCHO):
    def get_object_id(self) -> str:
        # needs to be string
        object_id = self.cho_data.get("ID-Nummer")
        return object_id

    def preprocess_data(self, data):
        preprocessed_data = self.split_mission21_values(data)
        return preprocessed_data

    def postprocess_data(self, data):
        if "hidden_geoObject" in data:
            # first we remove the {place} and similar content in the geoAnreicherung field
            data["hidden_geoObject"] = list(map(self.remove_curly_brackets, data["hidden_geoObject"]))
        # then we do standard enrichment
        return super().postprocess_data(data)

    # preprocessed_data is one record only
    def split_mission21_values(self, preprocessed_data):
        preprocessed_item = {}
        for key, value in preprocessed_data.items():
            if isinstance(value, list):
                preprocessed_item[key] = value
            elif key in self.dl_o.semikolon_fields and value:
                preprocessed_item[key] = [x.strip() for x in value.split(";")]
            else:
                preprocessed_item[key] = value.strip()
        return preprocessed_item

    @staticmethod
    def remove_curly_brackets(value: str):
        """
        Durban {place} will be shortened to Durban
        :param value: the value
        :return: the result
        """
        return re.sub(r'\{.*?\}', '', value).strip()


class RDVDataMission21():
    def __init__(self, logger, **kwargs):
        self.logger = logger
        self.file = kwargs["file"]
        # self.image_lookup = self.extract_imagepath_from_file(kwargs["img_lookup"])
        self.semikolon_fields = ["Autor", "Schlagwort", "Lokalisation", "Betrifft"]

    def get_data(self):
        csv_data = self.get_csv_data(self.file, encoding="utf-8", delimiter=",")
        csv_data = self.filter_africa_content(csv_data)
        for record in csv_data:
            record["type"] = ["mission21"]
            record["proj_id"] = "mission21"
            record["providing_institution"] = ["BM Archives"]
            country_name = self.get_country_based_on_call_number(record["Vollständige Signatur"])
            if country_name:
                record["country_name"] = self.get_country_based_on_call_number(record["Vollständige Signatur"])
        return csv_data

    def get_csv_data(self, filepath, encoding: str = "utf-8", delimiter=",") -> dict:
        data = []
        with open(filepath, encoding=encoding) as file_fh:
            for line in DictReader(file_fh, delimiter=delimiter):
                data.append(line)
        return data

    def filter_africa_content(self, data):
        """
        Mission 21 data contains all Mission 21 data. For Afrika-Portal, we need to import only the data related
        to Africa based on Signatures
        :param data: incoming data (all records from Mission21)
        :return: the data with non-africa content removed
        """
        return list(filter(self.is_africa_content, data))

    @staticmethod
    def is_africa_content(record):
        """
        Check if a record should be imported

        Bitte beachte, dass nicht das gesamte CSV importiert werden muss. Lediglich die Daten zu Afrika:
        Patrick Moser: “Bei den Bildern sowie den Karten und Plänen würde ich nicht nach der Signatur gehen, sondern nach den geografischen Schlagwörtern.”
        Bestand A Archives: Alle Signaturen, die mit D-, E-, EN-, FA- oder FF- beginnen
        D-
        D.
        E-
        E.
        EN
        FA
        FF


        Bestand BM Basler Mission ab 1955: Alle Signaturen, die mit 10. Beginnen
        BM.10

        Bestand Betriebsdokumentation, Unterbestand B1 Europäische Sprachen: Alle Signaturen, die mit D., E. oder E.N. beginnen (es hat in anderen Signaturen, zum Beispiel G. noch weitere vereinzelte Titel zu Afrika. Es wäre aber aufwändig, die einzeln herauszuholen).
        Bestand Betriebsdokumentation, Unterbestand B2 Übersee-Sprachen: Alle Signaturen, die mit D., E. oder E.N beginnen

        Bestand BMDZ Basler Mission Deutscher Zweig: Alle Signaturen, die mit I. beginnen
        BMDZ_I

        Bestand KEM Kooperation evangelischer Kirchen und Missionen: Alle Signaturen, die mit 03.02., 03.05 und 08. beginnen
        KEM-03.02
        KEM-03.05
        KEM.08

        Bestand BHG Basler Handelsgesellschaft / Basel Trading Company: Explizit Afrika betreffen alle Signaturen, die mit 13, 14.01 bis 14.07 beginnen. Da sich aber auch in anderen Signaturen Dokumente über den Handel in Afrika befinden, würde ich vorschlagen, den ganzen Bestand der Handelsgesellschaft ins Portal zu übernehmen.
        BHG

        Bestand B Maps and Plans: Sämtliche Karten, die das geografische Schlagwort „Africa (continent)“ aufweisen
        Bestand C Images: Sämtliche Bilder, die das geografische Schlagwort „Africa (continent)“ aufweisen
        Africa {continent}



        :param pair: a key value pair from a dictionary where value is the record
        :return: true / false
        """
        prefixes = [
            "D-",
            "D.",
            "E-",
            "E.",
            "EN",
            "FA",
            "FF",
            "BM.10",
            "BMDZ_I",
            "KEM-03.02",
            "KEM-03.05",
            "KEM.08",
            "BHG"
        ]

        # we only import ["Dokument", "Dossier", "Teildossier"] not "Bestand", "Unterbestand"...
        # cf. https://ub-basel.atlassian.net/browse/AP-147
        if record["Verzeichnungsstufe"] not in ["Dokument", "Dossier", "Teildossier"]:
            return False
        else:
            if record["Vollständige Signatur"].startswith(tuple(prefixes)):
                return True
            elif "Africa {continent}" in record["Schlagwort"]:
                return True
            else:
                return False

    @staticmethod
    def get_country_based_on_call_number(call_number: str):
        """
        :param call_number: the call number
        :return: the country name (in English), None if no country is recognized
        """
        if call_number.startswith("D") or call_number.startswith("BM.10.04") or call_number.startswith("BHG-13"):
            return "Ghana"
        elif call_number.startswith("EN") or call_number.startswith("BM.10.08"):
            return "Nigeria"
        elif call_number.startswith("E") or call_number.startswith("BM.10.05"):
            return "Cameroon"
        elif call_number.startswith("FA"):
            return "Liberia"
        elif call_number.startswith("FF") or call_number.startswith("BM.10.10"):
            return "Sudan"
        elif call_number.startswith("BM.10.06"):
            return "Kenya"
        elif call_number.startswith("BM.10.07"):
            return "Democratic Republic of the Congo"
        elif call_number.startswith("BM.10.11"):
            return "Tanzania"
        else:
            return None
