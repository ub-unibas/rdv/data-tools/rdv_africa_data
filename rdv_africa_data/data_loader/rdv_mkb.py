import re
from csv import DictReader

from rdv_africa_data.data_process import RDVCHO


class RDVCHO_MKB(RDVCHO):
    white_listed_gdoc_functions = set(["build_geo_object", "build_iiif_link", "extract_country_from_thesaurus"])

    def build_geo_object(self, value, es_field, orig_field):
        # todo: bessere Lösung als geo_done
        if not hasattr(RDVCHO_MKB, "geo_done"):
            # get all GEO_OBJECTS fields
            orig_fields_place = self._mapping.get_orig_fields(es_field)
            country, place = "", ""
            sub_enrich_field = "Herk/Bezirk/Distrikt"
            map_table = {"Herk.1: Ort": "country", "Herk.6: Land": "country",
                         "Bezirk/Gemeinde": "place", "Distrikt": "place"}

            # transform geo fields in subfields for geoenrichment
            for orig_field in orig_fields_place:
                geo_content = self.cho_data.get(orig_field, [])
                if geo_content and orig_field in map_table:
                    enrichment_sub_field = map_table[orig_field]
                    if enrichment_sub_field == "country":
                        # nur position 1 wird berücksichtig
                        country = geo_content.split(" / ")[0]
                    else:
                        place = geo_content
            self.geo_done = 1
            return GeoObject(place=place, country=country, sub_enrich_field=sub_enrich_field,
                             log_data=self._dataloader.log_data)

    def build_iiif_link(self, value, es_field, orig_field):
        return "mkb-" + value.replace("/", "-")

    def get_object_id(self) -> str:
        # needs to be string
        object_id = self.cho_data.get("Bitte beliebigen Zahlenwert eintragen")
        return object_id


class RDVDataMKB():

    def __init__(self, logger, **kwargs):
        self.logger = logger
        self.file = kwargs["file"]
        # self.image_lookup = self.extract_imagepath_from_file(kwargs["img_lookup"])

    def get_data(self):
        csv_data = self.get_csv_data(self.file, encoding="utf-8", delimiter=",")
        for record in csv_data:
            record["type"] = ["mkb"]
            record["proj_id"] = "mkb"
            record["providing_institution"] = ["MKB2022"]
        return csv_data

    def get_csv_data(self, filepath, encoding: str = "utf-8", delimiter=",") -> dict:
        data = []
        with open(filepath, encoding=encoding) as file_fh:
            for line in DictReader(file_fh, delimiter=delimiter):
                data.append(line)
        return data

    def extract_imagepath_from_file(self, file: str) -> dict:
        """ for mkb to extract image path from folder for lookup

        :param folder:
        :return:
        """
        folders_lookup = {}
        with open(file) as fh:
            for line in fh:
                line = line.strip()

                if re.search("(KK(\\d)?)|(_Verso).jpg", line) or re.search("III[ -]\\d{2,6}.jpg", line):
                    self.logger.info("Ignored: {}".format(line))
                    continue
                signatur = line.split("/")[-2]
                folders_lookup.setdefault(signatur, []).append(line)
        return folders_lookup
