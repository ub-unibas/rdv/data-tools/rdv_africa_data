import re
from csv import DictReader

from data_loader import utility
from media_server.media_server_client import MediaServerClient
from rdv_africa_data.data_process import RDVCHO


class RDVCHO_MKB(RDVCHO):
    def get_object_id(self) -> str:
        # needs to be string
        object_id = self.cho_data.get("Vom System vergebene Nr.")
        return object_id

    def preprocess_data(self, data):
        preprocessed_data = self.split_mkb_countries(data)
        preprocessed_data = self.split_mkb_subjects(preprocessed_data)
        return preprocessed_data

    def postprocess_data(self, data):
        # data is a single record
        data = super().postprocess_data(data)

        # add image info
        media_server_collection = self.dl_o.media_server_collection
        if "Inventarnummer" in data:
            signature = data["Inventarnummer"].lower()

            signature_image = signature.replace("iii ", "iii-")

            # replace ( and ) by underscore as ( and ) are not allowed as
            # media server signatures
            signature_image = signature_image.replace("(", "_").replace(")", "_")

            # the separator changes between quellnummer and the rest (could be _ . -  a b ...)
            relevant_images = self.extract_elements(self.dl_o.images, signature_image + "_")
            relevant_images.update(self.extract_elements(self.dl_o.images, signature_image + "."))
            relevant_images.update(self.extract_elements(self.dl_o.images, signature_image + "-"))
            relevant_images.update(self.extract_elements(self.dl_o.images, signature_image + "a"))
            relevant_images.update(self.extract_elements(self.dl_o.images, signature_image + "b"))

            counter = 0
            # sort images by page number
            relevant_images = sorted(relevant_images, key=self.extract_page_order)

            for key in relevant_images:
                data = self.add_one_image(data, media_server_collection, key, counter)
                counter = counter + 1
                break # we only add the first image https://ub-basel.atlassian.net/browse/AP-230

        return data

    @staticmethod
    def extract_page_order(text):
        """
        from page001 extract 1
        for the cover pages (cover1) assign a lower index
        for the rear pages (rear1) assign a higher index
        page: a string like page001 or K02_page13
        :return:
        """
        offset = 0
        # Assign a higher value to "rear" pages and a lower value to "cover" pages
        if "rear" in text:
            offset = 10000
        elif "cover" in text:
            offset = -10000

        # Extract the numeric part from the page number
        matches = re.findall(r'\d+', text)
        if matches:
            return offset + int(matches[-1])
        else:
            return 0

    @staticmethod
    def extract_elements(dictionary, prefix):
        """
        Return a dictionary with all the elements where the key starts with prefix
        :param dictionary:
        :param prefix:
        :return:
        """
        return {key: value for key, value in dictionary.items() if key.startswith(prefix)}

    def add_one_image(self, data, media_server_collection, signature, position):
        """
        Add one image to the manifest
        :param data: the record
        :param media_server_collection:
        :param signature:
        :return:
        """

        image = self.dl_o.images[signature]
        iiif_img = {}
        iiif_img["width"] = image["width"]
        iiif_img["height"] = image["height"]
        iiif_img["id"] = image["signature"]
        iiif_img["pos"] = position
        if "iiif_imgs" in data:
            data["iiif_imgs"].append(iiif_img)
        else:
            data["iiif_imgs"] = [iiif_img]
        # todo take this somewhere else at the end
        data["media_server_collection"] = media_server_collection
        data["hidden_digitized"] = "ja"

        return data

    def split_mkb_countries(self, preprocessed_data):
        """
        split countries on / (for example Togo / Südafrika) to create a list of countries

        :param preprocessed_data: input record
        :return: record with countries splitted at separator
        """
        if "Herk.6: Land" in preprocessed_data:
            if "/" in preprocessed_data["Herk.6: Land"]:
                preprocessed_data["Herk.6: Land"]=[x.strip() for x in preprocessed_data["Herk.6: Land"].split("/")]
        return preprocessed_data

    def split_mkb_subjects(self, preprocessed_data):
        """
        split subjectss on / or , because data is messy and contains
        Sarg / Mission / Prozession / fancy coffin / Figurensarg
        or
        Arkade, Blüte, Deir-el-Dyk, koptisch, Katalog Gayet 232

        :param preprocessed_data: input record
        :return: record with countries splitted at separator
        """
        if "Beschlagwortung" in preprocessed_data:
                if preprocessed_data["Beschlagwortung"].startswith("Einheimische Bezeichnung:"):
                    # we keep the subject as it is
                    preprocessed_data=preprocessed_data
                elif "/" in preprocessed_data["Beschlagwortung"]:
                    preprocessed_data["Beschlagwortung"]=[x.strip() for x in preprocessed_data["Beschlagwortung"].split("/")]
                elif "," in preprocessed_data["Beschlagwortung"]:
                    preprocessed_data["Beschlagwortung"]=[x.strip() for x in preprocessed_data["Beschlagwortung"].split(",")]
        if "Kultur" in preprocessed_data:
            if "/" in preprocessed_data["Kultur"]:
                preprocessed_data["Kultur"] = [x.strip() for x in preprocessed_data["Kultur"].split("/")]
        return preprocessed_data



class RDVDataMKB():

    def __init__(self, logger, **kwargs):
        self.logger = logger
        self.file_left = kwargs["file_left"]
        self.file_right = kwargs["file_right"]
        self.media_server_collection = kwargs["media_server_collection"]
        self.images = self.get_list_of_available_images(self.media_server_collection)
        # self.image_lookup = self.extract_imagepath_from_file(kwargs["img_lookup"])

    def get_data(self):
        csv_data = utility.merge_multiple_csv_files_based_on_id(sheet_left=self.file_left, sheet_right=self.file_right, common_column_name_left="Vom System vergebene Nr.", common_column_name_right="Bitte beliebigen Zahlenwert eintragen")
        for record in csv_data:
            record["type"] = ["mkb"]
            record["proj_id"] = "mkb"
            record["providing_institution"] = ["Museum der Kulturen Basel"]
        return csv_data

    def get_csv_data(self, filepath, encoding: str = "utf-8", delimiter=",") -> dict:
        data = []
        with open(filepath, encoding=encoding) as file_fh:
            for line in DictReader(file_fh, delimiter=delimiter):
                data.append(line)
        return data

    def get_list_of_available_images(self, collection):
        """
        Get all items from a collection in the media server and return a dictionary of dictionaries containing
        signature, width and height, with the signature as a key

        :param collection:
        :return:
        """
        media_server_client = MediaServerClient()
        items = media_server_client.get_items(collection=collection)

        results = {}
        for item in items:
            # todo check if key exists
            if "signature" in item:
                if "metadata" in item and "width" in item["metadata"] and "height" in item["metadata"]:
                    result = {
                        "signature": item["signature"],
                        "width": item["metadata"]["width"],
                        "height": item["metadata"]["height"]
                    }
                    results[item["signature"]] = result
                else:
                    self.logger.info("Missing metadata for image " + item["signature"])
            else:
                self.logger.info("Problem with media server")
        return results
