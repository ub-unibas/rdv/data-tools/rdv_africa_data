import csv
import io

from openrefine_loader.openrefine import OpenRefineES, OpenRefine
from values_normalizer_ubit import GeoData

from rdv_africa_data.data_process import RDVCHO


class RDVCHO_OpenRefine(RDVCHO):
    white_listed_gdoc_functions = set(["extract_geoname"])

    def extract_geoname(self, value, es_field, orig_field):
        countries = []
        for subject in value:
            if subject in GeoData.country_dict:
                countries.append(self.extractCountryCode(subject))
        return countries

    @staticmethod
    def extractCountryCode(country):
        country_codes = []
        country_entry = GeoData.country_dict.get(country, {"countryCode": ""})
        if country_entry["countryCode"]:
            country_codes.append(country_entry["countryCode"])
        return country_codes


class RDVDataOpenRefine():

    def __init__(self, logger, **kwargs):
        self.logger = logger
        data_path = dataset.settings_inst.data_path
        json_array = []

        # unterscheidung wann json wann csv Umwanldung
        or_project = OpenRefineES(data_path)
        columns_test = or_project.getProjColumns()

        # todo unterscheidung nicht genau genug
        if columns_test and columns_test[0].startswith("_ - "):
            or_json_repr = 1
        else:
            or_json_repr = 0

        if or_json_repr:
            json_data = OpenRefineES.getJsonData(export_project=or_project)
        else:
            or_project = OpenRefine(data_path)
            export_data = or_project.exportORefData()
            export_data = io.StringIO(export_data)
            json_data = {}
            last_id = ""
            for line in csv.DictReader(export_data):
                id_field = dataset.mapping.get_orig_fields(dataset.data_model.FIELD_ID)[0]
                # for records
                id_ = line.get(id_field, last_id) or last_id
                last_id = id_
                json_data.setdefault(id_, {})
                for key, value in line.items():
                    if value:
                        json_data[id_].setdefault(key, []).append(value)

        for key, line in json_data.items():
            json_array.append(line)
        return json_array
