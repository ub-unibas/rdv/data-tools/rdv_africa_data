from csv import DictReader

from rdv_africa_data.data_process import RDVCHO


class RDVCHOSwissTPH(RDVCHO):
    white_listed_gdoc_functions = set(["build_iiif_link"])

    def build_iiif_link(self, value, es_field, orig_field):
        return "swiss_tph-" + value.replace("/", "-")

    def preprocess_data(self, data):
        data = super().preprocess_data(data)
        preprocessed_item = self.split_stph_values(data)
        return preprocessed_item

    # data is one record only, should name it item or record
    def split_stph_values(self, data):
        result = {}
        for key, value in data.items():
            if isinstance(value, list):
                result[key] = value
            elif key in self.dl_o.semikolon_fields and value:
                # to do remove superfluous ; at the end
                stripped_value = value.strip(";")
                result[key] = [x.strip() for x in stripped_value.split(";")]
            else:
                result[key] = value.strip(";").strip()
        return result

    def get_object_id(self) -> str:
        # needs to be string
        object_id = self.cho_data.get("TPH Picture ID")
        return object_id


class RDVCHOSwissTPHProjects(RDVCHOSwissTPH):
    def get_object_id(self) -> str:
        # needs to be string
        object_id = self.cho_data.get("Projects - TiPPPs Project ID")
        # we use the filename of the image as an identifier, not great but there are no identifiers in the data
        return object_id


class RDVCHOSwissTPHFilms(RDVCHOSwissTPH):
    def get_object_id(self) -> str:
        # needs to be string
        object_id = self.cho_data.get("Access ID")
        # we use the filename of the image as an identifier, not great but there are no identifiers in the data
        return object_id


class RDVDataSwissTPH():

    def __init__(self, logger, **kwargs):
        self.logger = logger
        self.file = kwargs["file"]
        self.semikolon_fields = ["Subject", "Country", "Region", "Person", "Photographer"]

    def get_data(self):
        # todo : put encoding in config
        csv_data = self.get_csv_data(self.file, encoding="utf-8", delimiter=";")
        for record in csv_data:
            record["type"] = ["stph"]
            record["proj_id"] = "stph"
            record["providing_institution"] = ["Swiss TPH"]
        return csv_data

    def get_csv_data(self, filepath, encoding: str = "utf-8", delimiter=",") -> dict:
        data = []
        with open(filepath, encoding=encoding) as file_fh:
            for line in DictReader(file_fh, delimiter=delimiter):
                data.append(line)
        return data


class RDVDataSwissTPHProjects(RDVDataSwissTPH):

    def __init__(self, logger, **kwargs):
        self.logger = logger
        self.file = kwargs["file"]
        self.semikolon_fields = ["Projects - External Project Partners", "Projects - Involved Countries",
                                 "Projects Related - Topics", "Projects - Related Activities"]

    def get_data(self):
        # todo : put encoding in config
        csv_data = self.get_csv_data(self.file, encoding="utf-8", delimiter=";")
        for record in csv_data:
            record["type"] = ["stph_projects"]
            record["proj_id"] = "stph_projects"
            record["STPH_Type"] = "Project"
            record["providing_institution"] = ["Swiss TPH"]
            record["more_information"] = "https://www.swisstph.ch/en/projects"

        return csv_data


class RDVDataSwissTPHFilms(RDVDataSwissTPH):

    def __init__(self, logger, **kwargs):
        self.logger = logger
        self.file = kwargs["file"]
        self.semikolon_fields = ["Authors", "Photographer / Camera", "Keywords", "Country", "Place Region", "Persons"]

    def get_data(self):
        # todo : put encoding in config
        csv_data = self.get_csv_data(self.file, encoding="ISO-8859-1", delimiter=";")
        for record in csv_data:
            record["type"] = ["stph_films"]
            record["proj_id"] = "stph_films"
            record["STPH_Type"] = "Film"
            record["providing_institution"] = ["Swiss TPH"]

        return csv_data
