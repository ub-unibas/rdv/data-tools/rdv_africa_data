from csv import DictReader

from media_server.media_server_client import MediaServerClient
from rdv_africa_data.data_process import RDVCHO

from config import config


class RDVCHOSwissTPH(RDVCHO):

    def preprocess_data(self, data):
        data = super().preprocess_data(data)
        preprocessed_item = self.split_stph_values(data)
        return preprocessed_item

    def postprocess_data(self, data):
        # data is a single record
        data = super().postprocess_data(data)

        # add image info
        # we use the "Datei" field which has the name of the file
        if hasattr(self.dl_o, "media_server_collection"):
            media_server_collection = self.dl_o.media_server_collection
            if "Datei" in data:
                signature = data["Datei"].split(".")[0].lower()
                if signature in self.dl_o.images:
                    image = self.dl_o.images[signature]
                    iiif_img = {}
                    iiif_img["width"] = image["width"]
                    iiif_img["height"] = image["height"]
                    iiif_img["id"] = image["signature"]
                    iiif_img["pos"] = 0
                    data["iiif_imgs"] = [iiif_img]
                    # todo take this from config
                    data["media_server_collection"] = media_server_collection
                    data["hidden_digitized"] = "ja"
        return data

    # data is one record only, should name it item or record
    def split_stph_values(self, data):
        result = {}
        for key, value in data.items():
            if isinstance(value, list):
                result[key] = value
            elif key in self.dl_o.semikolon_fields and value:
                # to do remove superfluous ; at the end
                stripped_value = value.strip(";")
                if key == "Projects - Involved Countries":
                    # for stph project, countries have the following form MDG - Madagascar
                    countries_with_prefix = [x.strip() for x in stripped_value.split(";")]
                    countries = []
                    for country in countries_with_prefix:
                        if " - " in country:
                            countries.append(country.split(" - ")[1].replace(" (the)", ""))
                        else:
                            countries.append(country)
                    result[key] = countries
                else:
                    result[key] = [x.strip() for x in stripped_value.split(";")]
            else:
                result[key] = value.strip(";").strip()
        return result

    def get_object_id(self) -> str:
        # needs to be string
        object_id = self.cho_data.get("TPH Picture ID")
        return object_id


class RDVCHOSwissTPHProjects(RDVCHOSwissTPH):
    def get_object_id(self) -> str:
        # needs to be string
        object_id = self.cho_data.get("Projects - TiPPPs Project ID")
        # we use the filename of the image as an identifier, not great but there are no identifiers in the data
        return object_id


class RDVCHOSwissTPHFilms(RDVCHOSwissTPH):
    def get_object_id(self) -> str:
        # needs to be string
        object_id = self.cho_data.get("TPH Film ID")
        # we use the filename of the image as an identifier, not great but there are no identifiers in the data
        return object_id


class RDVDataSwissTPH():

    def __init__(self, logger, **kwargs):
        self.logger = logger
        self.file = kwargs["file"]
        self.semikolon_fields = ["Subject", "Country", "Region", "Person", "Photographer"]
        self.media_server_collection = kwargs["media_server_collection"]
        self.images = self.get_list_of_available_images(self.media_server_collection)

    def get_data(self):
        # todo : put encoding in config
        csv_data = self.get_csv_data(self.file, encoding="utf-8", delimiter=";")
        for record in csv_data:
            record["type"] = ["stph"]
            record["proj_id"] = "stph"
            record["providing_institution"] = ["Swiss TPH"]
        return csv_data

    def get_csv_data(self, filepath, encoding: str = "utf-8", delimiter=",") -> dict:
        data = []
        with open(filepath, encoding=encoding) as file_fh:
            for line in DictReader(file_fh, delimiter=delimiter):
                data.append(line)
        return data

    def get_list_of_available_images(self, collection):
        """
        Get all items from a collection in the media server and return a dictionary of dictionaries containing
        signature, width and height, with the signature as a key

        :param collection:
        :return:
        """
        media_server_client = MediaServerClient()
        items = media_server_client.get_items(collection=collection)

        results = {}
        for item in items:
            # todo check if key exists
            if "signature" in item:
                if "metadata" in item:
                    result = {
                        "signature": item["signature"],
                        "width": item["metadata"]["width"],
                        "height": item["metadata"]["height"]
                    }
                    results[item["signature"]] = result
                else:
                    self.logger.info("Missing metadata for image " + item["signature"])
            else:
                self.logger.info("Problem with media server")
        return results


class RDVDataSwissTPHProjects(RDVDataSwissTPH):

    def __init__(self, logger, **kwargs):
        self.logger = logger
        self.file = kwargs["file"]
        self.semikolon_fields = ["Projects - Project Partners", "Projects - Involved Countries",
                                 "Projects - Related Topics", "Projects - Related Activities"]

    def get_data(self):
        # todo : put encoding in config
        csv_data = self.get_csv_data(self.file, encoding="utf-8", delimiter=";")
        for record in csv_data:
            record["type"] = ["stph-projects"]
            record["proj_id"] = "stph-projects"
            record["STPH_Type"] = "Project"
            record["providing_institution"] = ["Swiss TPH"]
            record["more_information"] = "https://www.swisstph.ch/en/projects"

        return csv_data


class RDVDataSwissTPHFilms(RDVDataSwissTPH):

    def __init__(self, logger, **kwargs):
        self.logger = logger
        self.file = kwargs["file"]
        self.semikolon_fields = ["Authors", "Photographer / Camera", "Keywords", "Country", "Place Region", "Persons"]

    def get_data(self):
        # todo : put encoding in config
        csv_data = self.get_csv_data(self.file, encoding="utf-8", delimiter=";")
        for record in csv_data:
            record["type"] = ["stph-films"]
            record["proj_id"] = "stph-films"
            record["STPH_Type"] = "Film"
            record["providing_institution"] = ["Swiss TPH"]

        return csv_data
