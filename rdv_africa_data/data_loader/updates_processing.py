from datetime import datetime

from cache_decorator_redis_ubit import NoCache
from gdspreadsheets_ubit import authorize_pyg, PygExtendedClient


class UpdatesProcessing:
    def __init__(self, gd_service_account_environment_variable, logger, updates_processing_spreadsheet, gd_cache_decorator=NoCache):
        self.logger = logger
        self.updates_processing_spreadsheet = updates_processing_spreadsheet

        PygExtendedClient.gc = authorize_pyg(gd_service_account_env_var=gd_service_account_environment_variable,
                                             cache_decorator=gd_cache_decorator)
        self.gc = PygExtendedClient.gc

        spreadsheet = self.gc.open_by_key(self.updates_processing_spreadsheet)
        self.metadata_sheet = spreadsheet.worksheet_by_title("Metadaten")
        self.metadata_file_dict = self.metadata_sheet.gSpreadsheetKeyDict(key_field="Source")

        self.digital_objects_sheet = spreadsheet.worksheet_by_title("Digitalisate")
        self.digital_objects_dict = self.digital_objects_sheet.gSpreadsheetKeyDict(key_field="Source")

    def get_metadata_folder(self, project):
        try:
            return self.metadata_file_dict[project]["Folder"]
        except KeyError:
            self.logger.error("In Google Spreadsheet, missing Folder for project " + project)
            exit(1)

    def get_metadata_filename(self, project):
        try:
            return self.metadata_file_dict[project]["Metadata Filename"]
        except KeyError:
            self.logger.error("In Google Spreadsheet, missing Metadata Filename for project " + project)
            exit(1)

    def get_metadata_filename1(self, project):
        try:
            return self.metadata_file_dict[project]["File1 (MKB only)"]
        except KeyError:
            self.logger.error("In Google Spreadsheet, missing File1 (MKB only)")
            exit(1)

    def get_metadata_filename2(self, project):
        try:
            return self.metadata_file_dict[project]["File2 (MKB only)"]
        except KeyError:
            self.logger.error("In Google Spreadsheet, missing File2 (MKB only)")
            exit(1)

    def get_digital_objects_folder(self, project):
        try:
            return self.digital_objects_dict[project]["Folder"]
        except KeyError:
            self.logger.error("In Google Spreadsheet, missing Digitalisate Folder for project " + project)
            exit(1)

    def get_thesaurus_path(self, project):
        try:
            return self.metadata_file_dict[project]["Thesaurus (BAB Library only)"]
        except KeyError:
            self.logger.error("In Google Spreadsheet, missing Thesaurus")
            exit(1)

    def get_archive_thesaurus_path(self, project):
        try:
            return self.metadata_file_dict[project]["Thesaurus (BAB Archive only)"]
        except KeyError:
            self.logger.error("In Google Spreadsheet, missing Thesaurus")
            exit(1)

    def get_klassifikation_thesaurus_path(self, project):
        try:
            return self.metadata_file_dict[project]["Klassifikation Thesaurus (BAB archive)"]
        except KeyError:
            self.logger.error("In Google Spreadsheet, missing Klassifikation Thesaurus")
            exit(1)

    def write_last_run_data(self, project):

        # Get all values in the sheet
        metadata_spreadsheet_content = self.metadata_sheet.get_all_values()
        digital_objects_spreadsheet_content = self.digital_objects_sheet.get_all_values()

        # Find the row and column indices
        try:
            if project.endswith("-images"):
                col_index = digital_objects_spreadsheet_content[0].index("Last run date")  # First row contains column names
                row_index = [row[0] for row in digital_objects_spreadsheet_content].index(project)  # First column contains row names
                self.digital_objects_sheet.update_value((row_index + 1, col_index + 1), datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
            else:
                col_index = metadata_spreadsheet_content[0].index("Last run date")  # First row contains column names
                row_index = [row[0] for row in metadata_spreadsheet_content].index(project)  # First column contains row names
                self.metadata_sheet.update_value((row_index + 1, col_index + 1), datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        except ValueError:
            self.logger.error("Could not write back Last run date")
            self.logger.error("Row or column name not found! " + "Last run date /" + project)

        try:
            if project.endswith("-images"):
                col_index = digital_objects_spreadsheet_content[0].index("Must be updated (yes / no)")  # First row contains column names
                row_index = [row[0] for row in digital_objects_spreadsheet_content].index(project)  # First column contains row names
                self.digital_objects_sheet.update_value((row_index + 1, col_index + 1), "no")
            else:
                col_index = metadata_spreadsheet_content[0].index("Must be updated (yes / no)")  # First row contains column names
                row_index = [row[0] for row in metadata_spreadsheet_content].index(project)  # First column contains row names
                self.metadata_sheet.update_value((row_index + 1, col_index + 1), "no")
        except ValueError:
            self.logger.error("Could not write back Must be updated")
            self.logger.error("Row or column name not found! " + "Must be updated (yes / no) / " + project)

    def get_projects_to_process(self):
        """
        Return the list of projects that should be processed based on the column Must be updated (yes / no) from the spreadsheet.
        :return: Arrary of projects
        """
        try:
            metadata_to_update = [key for key, value in self.metadata_file_dict.items() if value.get("Must be updated (yes / no)") == "yes"]
            digital_images_to_update = [key for key, value in self.digital_objects_dict.items() if value.get("Must be updated (yes / no)") == "yes"]
            return metadata_to_update + digital_images_to_update
        except KeyError:
            self.logger.error("Must be updated (yes / no) column missing from spreadsheet.")
