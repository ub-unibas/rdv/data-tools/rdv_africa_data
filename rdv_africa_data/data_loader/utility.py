import os
import pandas as pd


def merge_multiple_csv_files(folder):
    """
    Takes all the csv files in a folder and merge them using the following rules (for example sheet1.csv and sheet2.csv)
    Take all columns from sheet1
    For sheet2 : if the column has the same header as in sheet1 add the field to the same column. If the column
     has a header which is not in sheet1 add this column to the result


    :param folder:
    :return: an array of dictionaries where each entry is a record
    """
    csv_files = [f for f in os.listdir(folder) if f.endswith(".csv")]

    csv_list = []
    for file in csv_files:
        csv_list.append(pd.read_csv(folder + "/" + file, encoding="ISO-8859-1", sep=";"))
    result = pd.concat(csv_list, ignore_index=True)
    # remove empty columns
    result.dropna(how="all", axis=1, inplace=True)
    # replace NaN by empty strings
    result = result.fillna("")
    # result.to_csv("result.csv")
    return result.to_dict(orient="records")

    # print(result.to_dict(orient="records"))


def merge_multiple_csv_files_based_on_id(sheet_left, sheet_right, common_column_name_left, common_column_name_right):
    """
    Merge 2 csv files as follows
    Take all columns from sheet_left
    Then enrich with the columns from sheet_right which are missing in sheet1

    :param sheet_left: base csv file
    :param sheet_right: csv file with additional columns
    :param common_column_name_left: name of the column in sheet_left where the common id is
    :param common_column_name_right: name of the column in sheet_right where the common id is
    :return: an array of dictionaries where each entry is a record
    """
    csv_file_1 = pd.read_csv(sheet_left, encoding="UTF-8", sep=";", dtype="str")
    csv_file_2 = pd.read_csv(sheet_right, encoding="UTF-8", sep=";", dtype="str")

    suffix = "_sheet2"

    merged_csv = csv_file_1.merge(csv_file_2, left_on=[common_column_name_left], right_on=[common_column_name_right], how="left", suffixes=(None, suffix))

    # remove the columns with the suffix _sheet2 and .1 (which are columns which are present multiple times)
    columns_to_keep = [c for c in merged_csv.columns if not (c.endswith(suffix) or c.endswith(".1"))]

    merged_csv = merged_csv[columns_to_keep]

    # remove empty columns
    merged_csv.dropna(how="all", axis=1, inplace=True)
    # replace NaN by empty strings
    merged_csv = merged_csv.fillna("")

    # define function to merge columns with same names together
    # def same_merge(x): return ','.join(x[x.notnull()].astype(str))

    # define new DataFrame that merges columns with same names together
    # merged_csv = merged_csv.groupby(level=0, axis=1).apply(lambda x: x.apply(same_merge, axis=1))
    # merged_csv.to_csv("result.csv")
    return merged_csv.to_dict(orient="records")

    # print(result.to_dict(orient="records"))
