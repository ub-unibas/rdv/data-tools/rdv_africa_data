import os
import pandas as pd


def merge_multiple_csv_files(folder):
    """
    Takes all the csv files in a folder and merge them using the following rules (for example sheet1.csv and sheet2.csv)
    Take all columns from sheet1
    For sheet2 : if the column has the same header as in sheet1 add the field to the same column. If the column
     has a header which is not in sheet1 add this column to the result


    :param folder:
    :return: an array of dictionaries where each entry is a record
    """
    csv_files = [f for f in os.listdir(folder) if f.endswith(".csv")]

    csv_list = []
    for file in csv_files:
        csv_list.append(pd.read_csv(folder + "/" + file, encoding="ISO-8859-1", sep=";"))
    result = pd.concat(csv_list, ignore_index=True)
    # remove empty columns
    result.dropna(how="all", axis=1, inplace=True)
    # replace NaN by empty strings
    result = result.fillna("")
    # result.to_csv("result.csv")
    return result.to_dict(orient="records")

    # print(result.to_dict(orient="records"))
