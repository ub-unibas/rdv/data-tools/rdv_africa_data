import json
import sys
from collections import Counter

from rdv_africa_data.config.constants import SHEET_NAME_MAPPING, SHEET_NAME_VIEW_MAPPING, \
    SHEET_NAME_DATAMODEL, KEY_COLUMN_MAPPING, KEY_ES
from gdspreadsheets_ubit import authorize_pyg, PygExtendedClient
from cache_decorator_redis_ubit import NoCache


class RDVGDocMapping:

    def __init__(self, gd_service_account_environment_variable, logger, data_model_spreadsheet, mapping_template_spreadsheet,
                 main_folder, gd_cache_decorator=NoCache, **kwargs):
        self._no_mapping = Counter()
        self.logger = logger
        self.mapping_template_spreadsheet = mapping_template_spreadsheet
        self.data_model_spreadsheet = data_model_spreadsheet
        self.main_folder = main_folder

        self.project = kwargs["project"]
        self.project_gaccount = kwargs["project_gaccount"]

        PygExtendedClient.gc = authorize_pyg(gd_service_account_env_var=gd_service_account_environment_variable, cache_decorator=gd_cache_decorator)
        self.gc = PygExtendedClient.gc
        if not kwargs["mapping_spreadsheet"]:
            self.mapping_spreadsheet = self.create_mapping_spreadsheet()
        else:
            self.mapping_spreadsheet = kwargs["mapping_spreadsheet"]
        self._mapping = self.transform_mapping2datamodel()

    def get_data_model_mapping(self):
        return self.transform_mapping2datamodel()

    def get_data_model(self):
        spreadsheet = self.gc.open_by_key(self.data_model_spreadsheet)
        datamodel_sheet = spreadsheet.worksheet_by_title(SHEET_NAME_DATAMODEL)
        data_model = datamodel_sheet.gSpreadsheetKeyDict(key_field=KEY_COLUMN_MAPPING)
        return data_model

    def get_mapping(self):
        spreadsheet = self.gc.open_by_key(self.mapping_spreadsheet)
        mapping_sheet = spreadsheet.worksheet_by_title(SHEET_NAME_MAPPING)
        mapping = mapping_sheet.gSpreadsheetKeyDict(key_field=KEY_COLUMN_MAPPING)
        return mapping

    def transform_mapping2datamodel(self):
        datamodel = self.get_data_model()
        mapping = self.get_mapping()
        mapping2datamodel = {}
        for orig_field_key, line in mapping.items():
            for field_label, v in line.items():
                if field_label == KEY_COLUMN_MAPPING:
                    continue
                datam_key = datamodel[field_label].get(KEY_ES)
                if not datam_key:
                    if field_label != "keine Zuordnung":
                        self.logger.warning("Für die Bezeichnung {} existiert kein Mapping im Spreadsheet Datenmodell {}"
                                            .format(field_label, self.data_model_spreadsheet))
                elif v:
                    mapping2datamodel.setdefault(orig_field_key, {}).setdefault(datam_key, {"mapping": v})
        return mapping2datamodel

    def create_mapping_spreadsheet(self):
        folder_exists = self.gc.drive.folder_metadata(query="parents in '{}' and name = '{}'".
                                                      format(self.main_folder, self.project))
        if not folder_exists:
            folder_id = self.gc.drive.create_folder(self.project, folder=self.main_folder)
            self.gc.drive.create_permission(folder_id, role="writer", type='user',
                                            emailAddress=self.project_gaccount)
            self.logger.info("Google Drive Folder {} {} with permissions for user {} created"
                             .format(self.project, folder_id, self.project_gaccount))
        else:
            folder_id = folder_exists[0].get("id")
            self.logger.debug("Google Drive Folder {} {} already exists"
                              .format(self.project, folder_id))

        spreadsheet_name = "Mapping {}".format(self.project)
        file_exists = self.gc.drive.spreadsheet_metadata(query="parents in '{}' and name = '{}'".
                                                         format(folder_id, spreadsheet_name))
        if not file_exists:
            template = self.gc.open_by_key(self.mapping_template_spreadsheet)
            spreadsheet = self.gc.create(title=spreadsheet_name,
                                         folder=folder_id, template=template)
            spreadsheet_id = spreadsheet.id
            self.logger.info("Google Spreadsheet {} {} created"
                             .format(spreadsheet_name, spreadsheet_id))
        else:
            spreadsheet_id = file_exists[0].get("id")
            self.logger.debug("Google Spreadsheet {} {} already exists"
                              .format(spreadsheet_name, spreadsheet_id))

        return spreadsheet_id

    def set_no_esmapping(self, orig_field_key: str) -> None:
        self._no_mapping[orig_field_key] += 1

    def get_esmapping_field(self, orig_field_key: str) -> []:
        return self._mapping.get(orig_field_key, [])

    def get_mapping_rule(self, orig_field_key: str, es_field: str) -> str:
        mapping_rule = self._mapping.get(orig_field_key, {}).get(es_field, {}).get("mapping", None)
        return mapping_rule

    def write_data2gd(self) -> None:
        spreadsheet = self.gc.open_by_key(self.mapping_spreadsheet)
        self.write_mapping2gd_with_matrix_sorting(spreadsheet, SHEET_NAME_MAPPING)
        self.write_mapping2gd_with_simple_sorting(spreadsheet, SHEET_NAME_VIEW_MAPPING)

    def update_existing_mapping(self, gd_sheet):
        # the table in google drive as a dictionary
        existing_mapping = gd_sheet.gSpreadsheetKeyDict(key_field=KEY_COLUMN_MAPPING)

        # we need to add the new fields to the spreadsheet
        # for this we check the fields which are not in the spreadsheet yet
        all_fields = self._no_mapping.keys() | self._mapping.keys()

        new_fields = all_fields - existing_mapping.keys()

        # add the new fields to the spreadsheets. Only the first column is filled, the rest is empty

        new_fields_dict = {}

        for key in new_fields:
            new_fields_dict[key] = {KEY_COLUMN_MAPPING: key}
        existing_mapping.update(new_fields_dict)
        return existing_mapping

    def write_mapping2gd_with_matrix_sorting(self, spreadsheet, sheetname):
        """
        Does matrix like sorting for the mapping table
        :param spreadsheet:
        :param sheetname:
        :return:
        """
        mapping_sheet = spreadsheet.worksheet_by_title(sheetname)
        headers = mapping_sheet.gSpreadsheetGetHeaders()

        def sort_mapping_lines(items: dict):
            # exclude first "Feld"-column sort after es_field name (column)
            key, value = items
            mapped_values = sorted(list(filter(lambda x: value[x] if x != KEY_COLUMN_MAPPING else "", value.keys())))
            field = mapped_values[0] if mapped_values else "keine Zuordnung"
            return field, key

        updated_mapping = self.update_existing_mapping(mapping_sheet)

        mapping_sheet.gSpreadsheetsSheetWriter(updated_mapping,
                                               key_field=KEY_COLUMN_MAPPING,
                                               sort_func=sort_mapping_lines,
                                               headers=headers,
                                               method="merge+overwrite")

        self.logger.info("Mapping written to Spreadsheet {} {}".format(spreadsheet.id, mapping_sheet.title))

    def write_mapping2gd_with_simple_sorting(self, spreadsheet, sheetname):
        '''
        Sort by field name only
        :param spreadsheet:
        :param sheetname:
        :return:
        '''
        mapping_sheet = spreadsheet.worksheet_by_title(sheetname)
        headers = mapping_sheet.gSpreadsheetGetHeaders()

        updated_mapping = self.update_existing_mapping(mapping_sheet)

        mapping_sheet.gSpreadsheetsSheetWriter(updated_mapping,
                                               key_field=KEY_COLUMN_MAPPING,
                                               headers=headers,
                                               method="merge+overwrite")

        self.logger.info("Mapping written to Spreadsheet {} {}".format(spreadsheet.id, mapping_sheet.title))
