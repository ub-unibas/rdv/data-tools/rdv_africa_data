from values_normalizer_ubit import GeoData, UnknownCountryException, GeoObject
from rdv_marc_ubit import GNDMarcJSONRecord
from gdspreadsheets_ubit import authorize_pyg, PygExtendedClient
from cache_decorator_redis_ubit import NoCache, create_cache_decorator

from config import config

import datetime


class RDVCHO:
    # delete fields / filter fields u. records
    white_listed_gdoc_functions = set()

    def __init__(self, dataloader_obj, cho_data, mapping_obj, enricher_obj):
        self.cho_data = cho_data
        self.dl_o = dataloader_obj
        self.m_o = mapping_obj
        self.e_o = enricher_obj
        self.logger = self.dl_o.logger

    def get_object_id(self) -> str:
        # to be implemented at data source level
        pass

    @staticmethod
    def _parse_fields(value, key=None) -> list:
        if isinstance(value, list):
            return value
        elif isinstance(value, str) or isinstance(value, int) or isinstance(value, float):
            return [value]
        elif value is None:
            return []
        else:
            return value

    def preprocess_data(self, data):
        return data

    def postprocess_data(self, data):
        # data is a single record
        if not GeoData.initialized:
            ap_config = config.get_config()
            if "redis_geonames" in ap_config:
                redis_geonames_config = ap_config["redis_geonames"]
                geonames_cache_decorator = create_cache_decorator(config_dict=redis_geonames_config)
                GeoData.initialize(geonames_cache_decorator=geonames_cache_decorator, geonames_username=ap_config["enrichment"]["geo_enrichment"]["geonames_username"])
            else:
                GeoData.initialize()  # init with NoCache for Geonames

        # add processing time
        data["processing_time"] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        """apply enrichment processes"""
        for key, value in data.items():
            if key != "alma_links" and not key.startswith("856"):
                # todo find a better way to to this
                # the alma links are already generated via get_portfolio
                data[key] = self._generate_links(value)
        if "hidden_dateObject" in data:
            enrich_process = self.e_o.get_enrichment_process("hidden_dateObject")
            if enrich_process:
                enriched_value = enrich_process(data["hidden_dateObject"])
                # integers
                data["fct_year"] = enriched_value
                # as string so that the old date facet still works
                data["fct_date"] = list(map(str, enriched_value))
        if "field_id" in data and "proj_id" in data:
            # to have unique manifest identifier, we combine the proj_id and the identifier defined in
            # the mappings in google drive for the id field
            data["field_id"] = [data["proj_id"] + "_" + x for x in data["field_id"]]
        if "fct_mediatype" in data:
            mediatype_normalizer = self.e_o.get_enrichment_process("fct_mediatype")
            if mediatype_normalizer:
                data["fct_mediatype"] = mediatype_normalizer(data["fct_mediatype"])
        if "fct_topic" in data:
            data["fct_topic"] = self._remove_question_marks_from_topics(data["fct_topic"])
        if "fct_topic" in data or "gnd_subject_identifiers" in data:
            spreadsheet_enricher = self.e_o.get_enrichment_process("fct_topic")
            gnd_identifiers = []
            # GND enrichment
            if data["proj_id"] == "africa_bsiz":  # case of alma where gnd id's are already available
                if "gnd_subject_identifiers" in data:
                    gnd_identifiers = data["gnd_subject_identifiers"]
            elif spreadsheet_enricher:
                gnd_identifiers = spreadsheet_enricher(data["fct_topic"])

            if self.e_o.get_enrichment_process("gnd_enrichment_from_mongo"):
                data["gnd_topic"] = []
                data["gnd_variants"] = []
                for gnd_identifier in gnd_identifiers:
                    if gnd_identifier:  # could be None
                        gnd_identifier = gnd_identifier.strip()
                        gnd_record = GNDMarcJSONRecord.get_gndid(gnd_id=gnd_identifier,
                                                                 cachedecorator=self.e_o.cachedecorator)
                        if gnd_record:
                            label = gnd_record.get_name_id_label()
                            if label:
                                data["gnd_topic"].append(label[0])
                                variant_names = gnd_record.get_field_data(["4[1350][10]"],
                                                                          ["a"])  # fields 450a, 400a, 410a, 451a...

                                # add gnd macs 750 a
                                variant_names.extend(gnd_record.get_field_data(["750"], ["a"]))

                                data["gnd_variants"].extend(variant_names)
                            else:
                                self.logger.info("No label for " + gnd_identifier)
                        else:
                            self.logger.info(gnd_identifier + " not found in mongo")
        if "gnd_topic" in data:
            gnd_translater = self.e_o.get_enrichment_process("gnd_translation")
            if gnd_translater:
                gnd_labels = [entry["label"] for entry in data["gnd_topic"]]
                data["gnd_translation"] = gnd_translater(gnd_labels)

        if "852_b_4" in data:
            alma_codes_mapper = self.e_o.get_enrichment_process("852_b_4")
            if alma_codes_mapper:
                alma_codes = [entry for entry in data["852_b_4"]]
                standort_labels = alma_codes_mapper(alma_codes)
                # remove duplicate labels
                data["852_b_4"] = list(dict.fromkeys(standort_labels))

        if "fct_location" in data:
            data["fct_location"] = self._remove_countries(data["fct_location"])
        if "fct_countryname" in data:
            data["fct_countrycodes"] = self._get_country_codes(data["fct_countryname"])
            data["fct_countryname"] = [
                GeoData.getGermanCountryLabel(country_code) for country_code in data["fct_countrycodes"]
            ]
        if self.e_o.get_enrichment_process("geo_enrichment") and ("fct_countryname" in data) and (data["fct_countryname"]):
            coordinates = []
            # enrich places with coordinates from geonames
            if "hidden_geoObject" in data:
                for place in data["hidden_geoObject"]:
                    place_geo = GeoObject(place=place, country_codes=data["fct_countrycodes"], logger=self.logger)
                    if not (place in GeoData.country_dict):  # when the place is only a country, we don't query geonames for it
                        place_geo.enrichBasedOnCountryCode()
                        # we only enrich coordinates from geonames for places (geonames feature class "P")
                        # we don't add coordinates for countries, states, rivers, lakes...
                        if (place_geo.is_enriched
                                and place_geo.fcl == "P"
                                and place_geo.geonames_id != 4385855  # city called Europa in US
                                and place_geo.geonames_id != 5145444):  # city called Africa in US
                            coordinates.append({
                                "location": place_geo.coords,
                                "label": place,
                                "geonames_label": place_geo.geonames_name,
                                "id": str(place_geo.geonames_id),
                                "feature_class": place_geo.fcl,
                                "feature_code": place_geo.fcode,
                                "score": place_geo.score,
                            })
            if coordinates:
                data["coordinates"] = coordinates
            # enrich countries with coordinates from geonames
            country_coordinates = []
            for country in data["fct_countryname"]:
                country_geo = GeoObject(place=country, country_codes=data["fct_countrycodes"])
                country_geo.enrichBasedOnCountryCode()
                # at this step we only enrich coordinates from geonames for countries (geonames feature class "P")
                # we don't add coordinates for countries, states, rivers, lakes...
                if country_geo.is_enriched and country_geo.fcode == "PCLI":
                    country_coordinates.append({
                        "location": country_geo.coords,
                        "label": country,
                        "geonames_label": country_geo.geonames_name,
                        "id": str(country_geo.geonames_id),
                        "feature_class": country_geo.fcl,
                        "feature_code": country_geo.fcode,
                        "score": country_geo.score,
                    })
                if country_coordinates:
                    data["country_coordinates"] = country_coordinates

        return data

    @staticmethod
    def _generate_links(value):
        """create a link structure for values starting with http"""
        if isinstance(value, str):
            # start with http and don't contain space
            if value.startswith("http") and " " not in value:
                return [{"link": value, "label": value}]
            else:
                return value
        if isinstance(value, list):
            digi_link_label = []
            for v in value:
                if isinstance(v, str) and v.startswith("http") and " " not in v:
                    digi_link_label.append({"link": v, "label": v})
                else:
                    digi_link_label.append(v)
            return digi_link_label

    @staticmethod
    def _get_country_codes(value):
        country_codes = []
        for v in value:
            try:
                # sometimes we have something like Mozambique (?), we then remove the (?)
                v = v.replace("(?)", "").strip()
                cc = GeoData.getGeonameCountryCode(v)
                country_codes.append(cc)
            except UnknownCountryException as e:
                print(e)
        return country_codes

    @staticmethod
    def _remove_question_marks_from_topics(value):
        topics = []
        for v in value:
            # sometimes we have something like Asante (?), we then remove the (?)
            # for Asante? as well
            v = v.replace("(?)", "").strip()
            v = v.replace("?", "").strip()
            topics.append(v)
        return topics

    @staticmethod
    def _remove_countries(value):
        results = []
        for v in value:
            if not GeoData.isCountry(v):
                results.append(v)
        return results

    def _map_cho_data(self) -> {}:
        preprocessed_data = self.preprocess_data(self.cho_data)

        mapped_data = {}
        for orig_field_key, values in preprocessed_data.items():
            es_fields = self.m_o.get_esmapping_field(orig_field_key)
            if es_fields and values:
                for es_field in es_fields:
                    mapping_rule = self.m_o.get_mapping_rule(orig_field_key, es_field)
                    mapped_values = self._apply_mapping_rule(mapping_rule, values, orig_field_key, es_field)
                    mapped_data.setdefault(es_field, []).extend(mapped_values)
            # count fields for which no mapping is applied
            elif values:
                self.m_o.set_no_esmapping(orig_field_key)
            if orig_field_key not in mapped_data and values:
                mapped_data[orig_field_key] = values

        postprocessed_data = self.postprocess_data(mapped_data)
        return postprocessed_data

    def _apply_mapping_rule(self, mapping_rule, values, orig_field_key: str, es_field: str) -> None:
        if mapping_rule == "1":
            return self._parse_fields(values)
        # this makes it possible to use function directly in the mapping table
        elif mapping_rule[0] == "$":
            mapping_rule = mapping_rule[1:]
            if mapping_rule in self.white_listed_gdoc_functions:
                try:
                    processed_value = self._apply_mapping_function(values, es_field, orig_field_key, mapping_rule)
                    return self._parse_fields(processed_value)
                except AttributeError:
                    self.logger.critical("Error Mapping Rule:", mapping_rule, self)
                    return []
            else:
                self.logger.warning("Function {} not white listed for class {}".format(mapping_rule, type(self)))

    def _apply_mapping_function(self, values, es_field: str, orig_field: str, mapping_rule: str) -> list:
        results = []

        if mapping_rule and hasattr(self, mapping_rule):
            process_function = getattr(self, mapping_rule)
            if isinstance(values, list):
                for value in values:
                    result_value = process_function(value, es_field, orig_field)
                    results.extend(self._parse_fields(result_value))
            else:
                result_value = process_function(values, es_field, orig_field)
                results.extend(self._parse_fields(result_value))
        return results
