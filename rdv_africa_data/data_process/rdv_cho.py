
class RDVCHO:
    # delete fields / filter fields u. records
    white_listed_gdoc_functions = set()

    def __init__(self, dataloader_obj, cho_data, mapping_obj, enricher_obj):
        self.cho_data = cho_data
        self.dl_o = dataloader_obj
        self.m_o = mapping_obj
        self.e_o = enricher_obj
        self.logger = self.dl_o.logger

    def get_object_id(self) -> str:
        # needs to be string
        object_id = str(self.cho_data.get(self._data_model.FIELD_ID, [""])[0])
        return object_id

    @staticmethod
    def _parse_fields(value, key=None) -> list:
        if isinstance(value, list):
            return value
        elif isinstance(value, str) or isinstance(value, int) or isinstance(value, float):
            return [value]
        elif value is None:
            return []
        else:
            return value

    def preprocess_data(self, data):
        return data

    def postprocess_data(self, data):
        """apply enrichment processes"""
        for key, value in data.items():
            enrich_process = self.e_o.get_enrichment_process(key)
            if enrich_process:
                enriched_value = enrich_process(value, data)
                data[key] = enriched_value
            data[key] = self._generate_links(value)
        return data

    @staticmethod
    def _generate_links(value):
        if isinstance(value, str):
            if value.startswith("http"):
                return [{"link": value, "label": value}]
            else:
                return value
        if isinstance(value, list):
            digi_link_label = []
            for v in value:
                if isinstance(v, str) and v.startswith("http"):
                    digi_link_label.append({"link": value, "label": value})
                else:
                    digi_link_label.append(v)
            return digi_link_label

    def _map_cho_data(self) -> {}:
        preprocessed_data = self.preprocess_data(self.cho_data)

        mapped_data = {}
        for orig_field_key, values in preprocessed_data.items():
            es_fields = self.m_o.get_esmapping_field(orig_field_key)
            if es_fields and values:
                for es_field in es_fields:
                    mapping_rule = self.m_o.get_mapping_rule(orig_field_key, es_field)
                    mapped_values = self._apply_mapping_rule(mapping_rule, values, orig_field_key, es_field)
                    mapped_data.setdefault(es_field, []).extend(mapped_values)
            # count fields for which no mapping is applied
            elif values:
                self.m_o.set_no_esmapping(orig_field_key)
            if orig_field_key not in mapped_data and values:
                mapped_data[orig_field_key] = values

        postprocessed_data = self.postprocess_data(mapped_data)
        return postprocessed_data

    def _apply_mapping_rule(self, mapping_rule, values, orig_field_key: str, es_field: str) -> None:
        if mapping_rule == "1":
            return self._parse_fields(values)
        # this makes it possible to use function directly in the mapping table
        elif mapping_rule[0] == "$":
            mapping_rule = mapping_rule[1:]
            if mapping_rule in self.white_listed_gdoc_functions:
                try:
                    processed_value = self._apply_mapping_function(values, es_field, orig_field_key, mapping_rule)
                    return self._parse_fields(processed_value)
                except AttributeError:
                    self.logger.critical("Error Mapping Rule:", mapping_rule, self)
                    return []
            else:
                self.logger.warning("Function {} not white listed for class {}".format(mapping_rule, type(self)))

    def _apply_mapping_function(self, values, es_field: str, orig_field: str, mapping_rule: str) -> list:
        results = []

        if mapping_rule and hasattr(self, mapping_rule):
            process_function = getattr(self, mapping_rule)
            if isinstance(values, list):
                for value in values:
                    result_value = process_function(value, es_field, orig_field)
                    results.extend(self._parse_fields(result_value))
            else:
                result_value = process_function(values, es_field, orig_field)
                results.extend(self._parse_fields(result_value))
        return results
