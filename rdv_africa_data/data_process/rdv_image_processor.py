import re
import datetime
import os
import pathlib
from urllib.request import urlopen

from media_server.media_server_client import MediaServerClient
from rdv_data_helpers_ubit import get_rdv_logger
from wand.image import Image


class RDVImageProcessor:

    def __init__(self):
        self.media_server_client = MediaServerClient()
        self.logger = get_rdv_logger(yaml_config="config/logging.yaml")
        self.accepted_extensions = [
            ".tif",
            ".tiff",
            ".dng",
            ".jpg",
            ".jpeg"
            ".tiff",
            ".psd"
        ]

    def import_images(self, folder_name, media_server_collection_name, project):
        """
        This import the images in folder_name (incl. sub-folders) in the media server under media_server_collection_name.
        By default, the signature will be the basename of an image (for 3446 for 3446.tif), but for some
        collections it needs to be adapted to match the metadata

        :param folder_name: the folder where the images are located
        :param media_server_collection_name: the collection name in the media server
        :param project : the project name (for example bablibrary-images), so that project based signature changes can be made
        :return:
        """
        description = "Collection for " + media_server_collection_name + " created " + datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        self.media_server_client.create_collection(media_server_collection_name, description)
        counter = 0
        folder = pathlib.Path(folder_name)
        files = list(folder.rglob("*"))  # all files in all sub-folders
        tif_files = []
        non_tif_files = []
        for file in files:
            # only process file which have an image extension and where the filename doesn't start with a .
            if file.is_file() and self.has_image_extension(file) and not (file.stem.startswith(".")):
                if self.is_tiff(file):
                    tif_files.append(file)
                else:
                    non_tif_files.append(file)

        # tif files have priority over the other image files if the same image is present multiple times
        # (e.g. 1234.jpg and 1234.tif)
        for file in tif_files:
            self.process_image(file, media_server_collection_name, project)
            counter = counter + 1
        for file in non_tif_files:
            self.process_image(file, media_server_collection_name, project)
            counter = counter + 1
        self.logger.info("{} images loaded in media server".format(counter))

    def process_image(self, file, media_server_collection_name, project):
        basename = os.path.basename(file)
        self.logger.info(basename)
        # the basename without extension and in lowercase, as the media server
        # signatures are always lowercase
        signature = file.stem.lower()
        signature = self.adapt_signature_to_match_metadata(file, signature, project)
        file = self.get_scaled_image_location(file, signature, project)
        image_uri = f"""file://{file}"""
        self.media_server_client.post_image(signature, image_uri, media_server_collection_name)

    def has_image_extension(self, file):
        """
        Check if a filename is an image based on the extension .jpg, .tif, ...
        :param filename: a string respresenting a filename, for example test.jpg
        :return: true/false
        """
        extension = pathlib.Path(file).suffix.lower()
        if extension in self.accepted_extensions:
            return True
        else:
            return False

    def is_tiff(self, file):
        """
        Check if a file has a .tif, .tiff, .TIF or .TIFF extension
        """
        extension = pathlib.Path(file).suffix.lower()
        if extension in [".tif", ".tiff"]:
            return True
        else:
            return False

    def adapt_signature_to_match_metadata(self, file, signature, project):

        # first replace parenthesis by underscores as they are not allowed as media server signatures
        # cf. https://ub-basel.atlassian.net/wiki/spaces/AF/pages/2050293774/UB+Medienserver+or+Media+server
        # Momentan sind folgende Symbole nicht erlaubt:
        # “ / ”, “\”, ”  # ”,  “?”,  “!”, “ “ “ (quote), “ ' “ (single quote), “ ` ” (backtick), "<", ">", "(", ")", "[", "]", “|", "¦", " ¨ " (diaeresis symbol)
        signature = signature.replace("(", "_").replace(")", "_")
        if project == "bablibrary-images" and "/rara_projekt/" in str(file):
            # Adds RP to the signature to match with the metadata
            return "rp " + signature
        elif project == "bablibrary-images" and "/poster_collection/" in str(file):
            # remove leading zeros as some files are named 00234.jpg but the signature in metadata will be 234
            signature = signature.lstrip("0")
            if not (signature.startswith("ex")):
                # we add X in front of the signature to match the records metadata (all signature starts with X in Plakatsammlung)
                # for the ePlakattsammlung it already has an EX in front
                return "x " + signature
            else:
                return signature
        else:
            return signature

    def get_scaled_image_location(self, file, signature, project):
        """
        Some images need to be scaled before being imported to the media server for copyright reasons

        :param file:
        :param project:
        :return:
        """
        if project == "bablibrary-images" and "/poster_collection/" in str(file):
            # if it is a poster (i.e. in the folder "Poster Collection") it needs to be scaled
            # cf. https://ub-basel.atlassian.net/wiki/spaces/AF/pages/741114525/BAB+Bin+rdaten
            # resize, keep aspect ratio, max(width, height) is 768 pixels
            # todo change hard-coded directory
            return self.scale_image(file, "/digiaccess/afrikaportal/bab/bab_library/resized_images/", "768x768>")
        elif project == "babarchive-images" and signature.startswith("baa_20_5"):
            # cf. https://ub-basel.atlassian.net/wiki/spaces/AF/pages/741114525/BAB+Bin+rdaten
            # todo change hard-coded directory
            return self.scale_image(file, "/digiaccess/afrikaportal/bab/bab_archive_resized_images/", "50%")
        else:
            return file

    def scale_image(self, source_file, target_directory, scaling_factor):
        """
        Takes the file under source_file, scale it according the scaling_factor (image_magick scaling syntax)
        and save it with the same name under target_directory
        :param source_file: the original file
        :param target_directory: the directory where the scaled file must be saved
        :param scaling_factor:  the scaling factor, for example "50%" or "768x768>"
                                cf. https://docs.wand-py.org/en/0.6.5/guide/resizecrop.html#transform-images
        :return: the location of the new file
        """
        updated_file_path = pathlib.Path(target_directory).joinpath(
            source_file.with_suffix('.jpg').name)
        if pathlib.Path.exists(updated_file_path):
            # if we already scale the file, we don't scale it again
            # to be faster
            return updated_file_path
        else:
            try:
                with Image(filename=source_file) as img:
                    img.transform(resize=scaling_factor)
                    img.format = "jpeg"  # convert to jpeg to write it correctly
                    # we will save the resized image on disk to upload this version to the media server
                    img.save(filename=updated_file_path)
                    return updated_file_path
            except Exception as e:
                self.logger.error("Problem with image resizing and uploading")
                self.logger.error(e)
