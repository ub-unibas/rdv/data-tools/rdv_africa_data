from rdv_africa_data.data_process import RDVCHO


class RDVDataMapper:

    def __init__(self, logger, dataloader_object, mapping_object, enricher_object, cho_class=RDVCHO):
        self.logger = logger
        self.cho_class = cho_class
        self.data_object = dataloader_object

        # Here we harvest the data (for example from the oai endpoint)
        self.data2map = dataloader_object.get_data()
        self.mapping_object = mapping_object
        self.enricher_object = enricher_object

    def map_data(self):
        mapped_data = {}
        if isinstance(self.data2map, dict):
            for obj_id, obj in self.data2map.items():
                rdvcho = self.cho_class(cho_data=obj, dataloader_obj=self.data_object, mapping_obj=self.mapping_object, enricher_obj=self.enricher_object)
                if obj_id not in mapped_data:
                    mapped_obj = rdvcho._map_cho_data()
                    mapped_data[obj_id] = mapped_obj
                else:
                    self.logger.critical("ID {} exists more than once within dataset".format(obj_id))
        elif isinstance(self.data2map, list):
            for obj in self.data2map:
                rdvcho = self.cho_class(cho_data=obj, dataloader_obj=self.data_object, mapping_obj=self.mapping_object, enricher_obj=self.enricher_object)
                obj_id = rdvcho.get_object_id()
                if obj_id not in mapped_data:
                    mapped_obj = rdvcho._map_cho_data()
                    mapped_data[obj_id] = mapped_obj
                else:
                    self.logger.critical("ID {} exists more than once within dataset".format(obj_id))
        self.mapping_object.write_data2gd()
        return mapped_data
