
import json
import yaml
import inspect

from rdv_africa_data.data_mapping import RDVGDocMapping
from rdv_africa_data.data_enrichment import RDV_Enrichment
from rdv_africa_data.data_ingest import RDVIngest
from rdv_africa_data.data_process import RDVDataMapper

# from openrefine_loader.openrefine import OpenRefine, OpenRefineES

from cache_decorator_redis_ubit import NoCacheDecorator


class RDVProcessData:

    def __init__(self, logger, config_file, project, dataloader_class, cho_class, mapping_class=RDVGDocMapping,
                 mapper_class=RDVDataMapper, enrichment_class=RDV_Enrichment, ingest_class=RDVIngest, gd_cachedecorator=NoCacheDecorator, data_cachedecorator=NoCacheDecorator):
        with open(config_file) as ap_config_fh:
            ap_config = yaml.load(ap_config_fh, Loader=yaml.BaseLoader)

        self.logger = logger
        self.project = project

        self.mapping_params = ap_config.get("mapping", {})
        self.elastic_params = ap_config.get("elasticsearch", {})
        self.gd_service_account_environment_variable = ap_config.get("google_drive", {}).get("service_account_environment_variable")
        self.gd_cachedecorator = gd_cachedecorator

        self.proj_params = ap_config.get("datasets", {}).get(self.project, {}).get("project")
        self.ingest_params = ap_config.get("datasets", {}).get(self.project, {}).get("ingest")
        self.process_params = ap_config.get("datasets", {}).get(self.project, {}).get("process")

        # dataloader object returns dictionary with obj-id as key : {obj_key1 : {"field1" :}}

        if inspect.isclass(dataloader_class):
            self.dataloader = dataloader_class(logger=logger, gd_service_file=self.gd_service_file,
                                               cachedecorator=data_cachedecorator, **self.process_params)
        else:
            self.dataloader = dataloader_class
        self.mapping = mapping_class(
            gd_service_account_environment_variable=self.gd_service_account_environment_variable,
            gd_cache_decorator=self.gd_cachedecorator,
            logger=logger,
            **self.mapping_params,
            **self.process_params,
            **self.proj_params)

        self.enricher = enrichment_class(logger=logger, cachedecorator=self.gd_cachedecorator, config=ap_config)
        self.ingest_class = ingest_class

        # harvest the data
        self.mapper = mapper_class(logger=logger, dataloader_object=self.dataloader, mapping_object=self.mapping,
                                   enricher_object=self.enricher, cho_class=cho_class)

    def map_data(self):
        mapped_data = self.mapper.map_data()
        return mapped_data

    def ingest_data2es(self, data):
        ingest = self.ingest_class(**self.elastic_params, **self.ingest_params, data=data)
        ingest.ingest_data()
        self.logger.warning(
            "{} records ingested in Elasticsearch.".format(len(data)))

    # def ingest_data2or(self, data):
    #     headers = ["field_id", "fct_location"]
    #     options_json = {"recordPath": ["_", "_"], "limit": -1, "trimStrings": False,
    #                     "guessCellValueTypes": False,
    #                     "storeEmptyStrings": True, "includeFileSources": False, "projectName": self.project}
    #     options_create = {"format": "text/json", "options": json.dumps(options_json)}
    #     or_project = OpenRefine.createNewORefProj(self.project, data, type="ap_json", headers=headers,
    #                                               options_create=options_create)
    #     return or_project

    # def get_datafromOR(self, or_id):
    #     export_data = OpenRefineES.getJsonData(name_id=or_id)
    #     return export_data

    def enrich_with_or_data(self, mapped_data, or_data):
        for key, obj in mapped_data.items():
            or_obj = or_data.get(key)
            if or_obj:
                mapped_data.update(or_obj)
        return mapped_data

    def apply_rules(self, old_or_proj_id=None, cache_id=None):
        pass
