
import json
import yaml
import inspect

import config.config
from rdv_africa_data.data_mapping import RDVGDocMapping
from rdv_africa_data.data_enrichment import RDV_Enrichment
from rdv_africa_data.data_ingest import RDVIngest
from rdv_africa_data.data_process import RDVDataMapper

# from openrefine_loader.openrefine import OpenRefine, OpenRefineES

from cache_decorator_redis_ubit import NoCache


class RDVProcessData:

    def __init__(self, logger, project, dataloader_class, cho_class, mapping_class=RDVGDocMapping,
                 mapper_class=RDVDataMapper, enrichment_class=RDV_Enrichment, ingest_class=RDVIngest, gd_cachedecorator=NoCache, data_cachedecorator=NoCache):

        ap_config = config.config.get_config()

        self.logger = logger
        self.project = project

        self.mapping_params = ap_config.get("mapping", {})
        self.elastic_params = ap_config.get("elasticsearch", {})
        self.gd_service_account_environment_variable = ap_config.get("google_drive", {}).get("service_account_environment_variable")
        self.gd_cachedecorator = gd_cachedecorator

        self.proj_params = ap_config.get("datasets", {}).get(self.project, {}).get("project")
        self.ingest_params = ap_config.get("datasets", {}).get(self.project, {}).get("ingest")
        self.process_params = ap_config.get("datasets", {}).get(self.project, {}).get("process")

        # dataloader object returns dictionary with obj-id as key : {obj_key1 : {"field1" :}}

        if inspect.isclass(dataloader_class):
            self.dataloader = dataloader_class(logger=logger, gd_service_file=self.gd_service_file,
                                               cachedecorator=data_cachedecorator, **self.process_params)
        else:
            self.dataloader = dataloader_class
        self.mapping = mapping_class(
            gd_service_account_environment_variable=self.gd_service_account_environment_variable,
            gd_cache_decorator=self.gd_cachedecorator,
            logger=logger,
            **self.mapping_params,
            **self.process_params,
            **self.proj_params)

        self.enricher = enrichment_class(logger=logger, project=project, cachedecorator=self.gd_cachedecorator, config=ap_config)
        self.ingest_class = ingest_class

        # harvest the data
        self.mapper = mapper_class(logger=logger, dataloader_object=self.dataloader, mapping_object=self.mapping,
                                   enricher_object=self.enricher, cho_class=cho_class)

    def map_data(self):
        mapped_data = self.mapper.map_data()
        return mapped_data

    def ingest_data2es(self, data):
        ingest = self.ingest_class(**self.elastic_params, **self.ingest_params, data=data)
        ingest.ingest_data()
        self.logger.warning(
            "{} records ingested in Elasticsearch.".format(len(data)))
