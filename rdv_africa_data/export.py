import numpy as np
import pandas
from elasticsearch import Elasticsearch
from elasticsearch import helpers
import json
import sys
import time
import io


# make an API call to the Elasticsearch cluster to get documents
print("\ncreating client instance of Elasticsearch")
elastic_client = Elasticsearch("localhost:9209")


# total num of Elasticsearch documents to get with API call
total_docs = 20

query_all = {
    "query": {"match_all": {}}
}

query = {
    "query": {
        "ids": {
            "values": ["bablibrary_001621b0-7ba1-5101-9603-eacbe6f9b9d5", "swisscovery_9930561710105504", "mkb_225886"]
        }
    }
}

query_specific_type = {
    "query": {
        "match": {
            "type": "foerster"
        }
    }
}


fields_to_keep = [
    "field_title",
    "fct_institution",
    "hidden_digitized",
    "field_id",
    "coordinates",
    "fct_location",
    "fct_mediatype",
    "fct_countryname",
    "fct_year",
    "gnd_topic",
    "gnd_translation",
    "fct_person_organisation",
    "fct_topic",
]


#  create an empty Pandas DataFrame object for docs
docs = pandas.DataFrame()


# iterate each Elasticsearch doc in list
print("\ncreating objects from Elasticsearch data.")
i = 0
for doc in helpers.scan(elastic_client, index='afrikaportal', query=query_all):
    # get _source data dict from document
    source_data_full = doc["_source"]

    # remove useless fields
    source_data = {key: source_data_full[key] for key in fields_to_keep if key in source_data_full}

    # get _id from document
    _id = doc["_id"]
    i = i + 1
    print(i)

    # if digitized add manifest url
    if "hidden_digitized" in source_data and "field_id" in source_data and source_data["hidden_digitized"] == "ja":
        source_data["iiif_manifest"] = "https://prod.es-iiif-service.ub-digitale-dienste.k8s-001.unibas.ch/afrikaportal/" + source_data["field_id"][0] + "/manifest/"

    source_data["parc_portal_url"] = "https://parc-portal.org/en/detail/" + _id

    # create a Series object from doc dict object
    doc_data = pandas.Series(source_data, name=_id)

    # append the Series object to the DataFrame object
    docs = docs._append(doc_data)

"""
EXPORT THE ELASTICSEARCH DOCUMENTS PUT INTO
PANDAS OBJECTS
"""
print("\nexporting Pandas objects to different file types.")

# export the Elasticsearch documents as a JSON file
docs.to_json("parc-portal.json", orient='index', force_ascii=False, indent=4)


# export Elasticsearch documents to a CSV file
docs.to_csv("parc-portal.csv", ",")  # CSV delimited by commas
