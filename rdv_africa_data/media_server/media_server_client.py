"""
Client for the UB Media Server
Documentation is here https://ub-basel.atlassian.net/wiki/spaces/AF/pages/2050293774/UB+Media+Server
"""
import os

import requests as requests
from rdv_africa_data.config.config import get_config
from rdv_data_helpers_ubit import get_rdv_logger


class MediaServerClient:
    def __init__(self):
        config = get_config()
        bearer_token = os.getenv("MEDIA_SERVER_BEARER_TOKEN")
        if bearer_token is None:
            raise Exception("Missing environment variable: MEDIA_SERVER_BEARER_TOKEN")
        self.session = requests.Session()
        self.session.headers.update(
            {
                "Accept": "application/json",
                "Content-Type": "application/json",
                "Authorization": "Bearer " + bearer_token
            }
        )
        self.media_server_url = config["media_server"]["host"]
        self.logger = get_rdv_logger(yaml_config="config/logging.yaml")
        self.should_retrieve_images = config.get("enrichment", {}).get("retrieve_images", True)

    def get_items(self, collection):
        """
        Get all item from a collection
        :param collection:
        :return:
        """
        endpoint = f"/api/collections/{collection}/items"
        params = {'offset': 0}

        results = []

        if not(self.should_retrieve_images):
            return results

        try:
            response = self.send_get_request(endpoint, params=params)
            results.extend(response["Items"])
            offset = response["Offset"]
            count = response["Count"]
            self.logger.info("Fetched {} of {} images".format(offset, count))
            while offset < count:
                params = {'offset': offset}
                response = self.send_get_request(endpoint, params=params)
                results.extend(response["Items"])
                offset = response["Offset"]
                count = response["Count"]
                self.logger.info("Fetched {} of {} images".format(offset, count))
            return results

        except Exception:
            # todo raise exception or return empty collections
            self.logger.error("No items returned for the collection " + collection)
            return results
            # return [{"foo": "bar"}]

    def post_image(self, signature, image_uri, collection):
        """
        Post an image to a collection
        :signature the signature to use in the media server
        :image_uri url or path to image (for example file:///digiaccess/afrikaportal/swiss-tph/Digitalisate/Bilder/sti0013.jpg)
        :param collection: the collection
        :return: nothing
        """
        endpoint = "/api/items"
        payload = f"""
            {{
               "signature": "{signature}",
               "source": "{image_uri}",
               "collectionname": "{collection}",
               "async": true,
               "public": true,
               "iiif": true
            }}
            """
        self.send_post_request(endpoint, payload)

    def create_collection(self, collection, collection_description):
        """
        Create a new collection in the media server using the estate "afrikaportal" and the storage "afrikaportal"
        :param collection: the collection
        :return: nothing
        """
        endpoint_create_collection = "/api/estates/afrikaportal/collections"
        create_collection_payload = f"""
            {{
                "storagename": "afrikaportal",
                "name": "{collection}",
                "description": "{collection_description}",
                "signatureprefix": "",
                "jwtKey": ""
            }}
            """
        self.send_post_request(endpoint_create_collection, create_collection_payload)

    def send_post_request(self, endpoint, data):
        try:
            response_media_server = self.session.request("POST", self.media_server_url + endpoint, data=data, verify=False)
        except requests.exceptions.RequestException as e:
            self.logger.error(e)
            self.logger.error("Problem with media server")
        else:
            if response_media_server.status_code != 200:
                self.logger.error(response_media_server.text)
            elif "status" in response_media_server.json() and response_media_server.json()["status"] == 1:
                self.logger.error(response_media_server.text)
            else:
                return response_media_server.json()

    def send_get_request(self, endpoint, params):
        try:
            response_media_server = self.session.request("GET", self.media_server_url + endpoint, params=params, verify=False)
        except requests.exceptions.RequestException:
            self.logger.error("Problem with media server")
        else:
            if response_media_server.status_code != 200:
                self.logger.error(response_media_server.text)
            elif "status" in response_media_server.json() and response_media_server.json()["status"] == 1:
                self.logger.error(response_media_server.text)
            else:
                return response_media_server.json()
