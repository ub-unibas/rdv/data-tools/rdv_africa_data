import os

from data_loader.updates_processing import UpdatesProcessing
from data_process import RDVImageProcessor

import config.config
from data_loader.rdv_mission21 import RDVDataMission21, RDVCHO_Mission21
from rdv_africa_data.data_loader.rdv_mkb import RDVDataMKB, RDVCHO_MKB
from rdv_africa_data.data_loader.rdv_swisstph import RDVDataSwissTPH, RDVCHOSwissTPH, RDVCHOSwissTPHProjects, \
    RDVDataSwissTPHProjects, RDVDataSwissTPHFilms, RDVCHOSwissTPHFilms
from rdv_africa_data.data_process import RDVProcessData

from rdv_africa_data.data_loader.rdv_foerster import RDVDataFoerster, RDVCHOFoerster

from rdv_africa_data.data_loader import RDVCHO_BSIZ, UBSAfricaOAI, RDVDataBABArchive
from rdv_africa_data.data_loader import RDVDataBABLibrary, RDVCHO_BAB

from rdv_data_helpers_ubit import get_rdv_logger

from rdv_marc_ubit import AlmaIZMarcJSONRecord, MarcTransformRule

from values_normalizer_ubit import GeoData

from cache_decorator_redis_ubit import NoCache
from cache_decorator_redis_ubit import create_cache_decorator
from mongo_decorator_ubit import create_mongo_decorator

import gdspreadsheets_ubit

# projects = []
#
# if os.getenv("PROJECTS") == "all":
#     projects = [
#         "bablibrary",
#         "mkb",
#         "mission21",
#         "stph-projects",
#         "stph-films",
#         "bsizdata",
#         "stph",
#         "babarchive",
#         "foerster",
#     ]
# else:
#     projects = os.getenv("PROJECTS").split(",")
#
# if os.getenv("RDV_DEVELOPMENT") == "1":
#     # projects = ["mkb", "babarchive", "stph", "bablibrary", "mission21"]
#     # projects = ["stph-images", "mkb-images", "foerster-images", "babarchive-images", "bablibrary-images", "babarchive",
#     #             "bablibrary", "stph", "mkb", "mission21", "foerster"]
#     # projects = ["bablibrary-images"]
#
#     # projects = ["bablibrary-images", "bablibrary"]
#
#     # projects = ["stph-images"]
#     projects = ["stph"]
#     # projects = ["babarchive"]
#     # projects = ["babarchive"]
#     # projects = ["bsizdata"]
#     # projects = ["stph"]
#     # projects = ["foerster"]
#     # projects = ["mkb"]
#     # projects = ["babarchive-images"]


ap_config = config.config.get_config()

gd_service_account_environment_variable = ap_config.get("google_drive", {}).get("service_account_environment_variable")
marc_spreadsheet = ap_config.get("datasets", {}).get("bsizdata", {}).get("process", {}).get("marc_process_spreadsheet")

updates_processing_spreadsheet = ap_config.get("datasets", {}).get("updates_processing_spreadsheet", {})
gdspreadsheets_ubit.authorize_pyg(gd_service_account_env_var=gd_service_account_environment_variable)


cache_decorator_afrikaportal = NoCache
if "redis" in ap_config:
    redis_config = ap_config["redis"]
    cache_decorator_afrikaportal = create_cache_decorator(config_dict=redis_config)

# create Decorator for Mongo (for example to store OAI-Records)
mongo_config = {"host": ap_config["mongo"]["host"], "port": ap_config["mongo"]["port"]}
mongo_decorator = create_mongo_decorator(config_dict=mongo_config, user=os.getenv("MONGO_USER", default=""), password=os.getenv("MONGO_PWD", default=""))

image_processor = RDVImageProcessor()
logger = get_rdv_logger(yaml_config="config/logging.yaml")
updates_processing = UpdatesProcessing(gd_service_account_environment_variable, logger, updates_processing_spreadsheet)

projects = updates_processing.get_projects_to_process()


for project in projects:
    logger.info("------------------------------------")
    logger.info("Start processing project " + project)
    logger.info("------------------------------------")
    if project == "bsizdata":
        # get Transformation Rule for Marc-Records
        marc_rule = MarcTransformRule(
            marc_spreadsheet=marc_spreadsheet,
            gd_service_file="",
            gd_service_account_env_var=gd_service_account_environment_variable,
            marc_record_class=AlmaIZMarcJSONRecord,
            gd_cache_decorator=NoCache)

        # specify database and collection
        # todo move to config the db_name and the collection_name
        slsp_oai_store = mongo_decorator(db_name="slsp_oai_harvesting", collection_name="africa")

        # build oai Harvester for SLSP africa-set
        # ES tunnel for port to ingest data into ES-> localhost:9209
        # incremental=True would only harvest new / changed records
        # incremental=False & Reharvest=False, would load old records from cache and new records
        # since last_harvest_date or if not set
        # from last stored date for harvesting from mongo db
        # reharvest=True: would reharvest the whole data_set and overwrite all
        # data within in mongo db

        # Initialize OAI Harvester
        ubsafrica_oai_harvester = UBSAfricaOAI(
            oai_set="africa",
            oai_metadata="marc21",
            oai_sets={},
            cachedecorator=cache_decorator_afrikaportal,
            oai_store=slsp_oai_store,
            marc_rule=marc_rule,
            logger=logger,
            es_host="this-is-not-used",
            incremental=ap_config["datasets"]["bsizdata"]["process"]["incremental"],
            reharvest=ap_config["datasets"]["bsizdata"]["process"]["reharvest"],
            last_harvest_date=ap_config.get("datasets", {}).
                    get("bsizdata", {}).get("process", {}).get("last_harvest_date"))

        # Harvest data
        rdv_bsizdata_processor = RDVProcessData(
            logger=logger,
            project=project,
            dataloader_class=ubsafrica_oai_harvester,
            cho_class=RDVCHO_BSIZ)
        # Map data (using google spreadsheets, write results back to google drive)
        bsiz_data = rdv_bsizdata_processor.map_data()
        # show mapped data
        # print(bsiz_data)
        # TODO: incremental ingest Parameter needs to be passed, can be done via ap_config_prod.yaml, without passing incremental=True only full ingest possible
        # ingest data in elasticsearch
        rdv_bsizdata_processor.ingest_data2es(bsiz_data)
        updates_processing.write_last_run_data(project)
    elif project == "mkb":

        # Initialize RDVDataMKB

        file_path_left = updates_processing.get_metadata_filename2(project=project)
        file_path_right = updates_processing.get_metadata_filename1(project=project)

        mkb_dataloader = RDVDataMKB(logger=logger,
                                    file_left=file_path_left,
                                    file_right=file_path_right,
                                    media_server_collection=ap_config["datasets"][project]["process"]["media_server_collection"]
                                    )

        rdv_mkb_processor = RDVProcessData(
            logger=logger,
            project=project,
            dataloader_class=mkb_dataloader,
            cho_class=RDVCHO_MKB)
        # Map data (using google spreadsheets, write results back to google drive)
        mkb_data = rdv_mkb_processor.map_data()
        rdv_mkb_processor.ingest_data2es(mkb_data)
        updates_processing.write_last_run_data(project)
    elif project == "bablibrary":
        folder = updates_processing.get_metadata_folder(project=project)
        thesaurus_path = updates_processing.get_thesaurus_path(project=project)

        bab_library_dataloader = RDVDataBABLibrary(logger=logger,
                                                   folder=folder,
                                                   thesaurus_filename=thesaurus_path,
                                                   media_server_collection=ap_config["datasets"][project]["process"]["media_server_collection"]
                                                   )

        rdv_bablibrary_processor = RDVProcessData(
            logger=logger,
            project=project,
            dataloader_class=bab_library_dataloader,
            cho_class=RDVCHO_BAB
        )
        bablibrary_data = rdv_bablibrary_processor.map_data()

        rdv_bablibrary_processor.ingest_data2es(bablibrary_data)
        updates_processing.write_last_run_data(project)

    elif project == "babarchive":
        folder = updates_processing.get_metadata_folder(project=project)
        thesaurus_path = updates_processing.get_archive_thesaurus_path(project=project)
        thesaurus_klassifikation_path = updates_processing.get_klassifikation_thesaurus_path(project=project)

        bab_archive_dataloader = RDVDataBABArchive(logger=logger,
                                                   folder=folder,
                                                   thesaurus_filename=thesaurus_path,
                                                   thesaurus_klassifikation_filename=thesaurus_klassifikation_path,
                                                   media_server_collection=ap_config["datasets"][project]["process"]["media_server_collection"]
                                                   )
        rdv_babarchive_processor = RDVProcessData(
            logger=logger,
            project=project,
            dataloader_class=bab_archive_dataloader,
            cho_class=RDVCHO_BAB)
        babarchive_data = rdv_babarchive_processor.map_data()

        rdv_babarchive_processor.ingest_data2es(babarchive_data)
        updates_processing.write_last_run_data(project)
    elif project == "stph":

        file_path = updates_processing.get_metadata_filename(project=project)

        stph_dataloader = RDVDataSwissTPH(logger=logger, file=file_path, media_server_collection=ap_config["datasets"][project]["process"]["media_server_collection"])

        rdv_stph_processor = RDVProcessData(
            logger=logger,
            project=project,
            dataloader_class=stph_dataloader,
            cho_class=RDVCHOSwissTPH)
        # Map data (using google spreadsheets, write results back to google drive)
        stph_data = rdv_stph_processor.map_data()
        rdv_stph_processor.ingest_data2es(stph_data)
        updates_processing.write_last_run_data(project)

    elif project == "stph-projects":
        projects_file_path = updates_processing.get_metadata_filename(project=project)

        stph_dataloader_projects = RDVDataSwissTPHProjects(logger=logger, file=projects_file_path)

        rdv_stph_processor_projects = RDVProcessData(
            logger=logger,
            project=project,
            dataloader_class=stph_dataloader_projects,
            cho_class=RDVCHOSwissTPHProjects)
        # Map data (using google spreadsheets, write results back to google drive)
        stph_data_projects = rdv_stph_processor_projects.map_data()

        rdv_stph_processor_projects.ingest_data2es(stph_data_projects)
        updates_processing.write_last_run_data(project)
    elif project == "stph-films":
        films_file_path = updates_processing.get_metadata_filename(project=project)

        stph_dataloader_films = RDVDataSwissTPHFilms(logger=logger, file=films_file_path)

        rdv_stph_processor_films = RDVProcessData(
            logger=logger,
            project=project,
            dataloader_class=stph_dataloader_films,
            cho_class=RDVCHOSwissTPHFilms)
        # Map data (using google spreadsheets, write results back to google drive)
        stph_data_films = rdv_stph_processor_films.map_data()

        rdv_stph_processor_films.ingest_data2es(stph_data_films)
        updates_processing.write_last_run_data(project)

    elif project == "foerster":

        folder = file_path = updates_processing.get_metadata_folder(project=project)

        foerster_dataloader = RDVDataFoerster(logger=logger, folder=folder,
                                              media_server_collection=ap_config["datasets"][project]["process"]["media_server_collection"])

        rdv_foerster_processor = RDVProcessData(
            logger=logger,
            project=project,
            dataloader_class=foerster_dataloader,
            cho_class=RDVCHOFoerster)
        # Map data (using google spreadsheets, write results back to google drive)
        foerster_data = rdv_foerster_processor.map_data()
        rdv_foerster_processor.ingest_data2es(foerster_data)
        updates_processing.write_last_run_data(project)

    elif project == "mission21":
        file_path = updates_processing.get_metadata_filename(project=project)

        mission21_dataloader = RDVDataMission21(logger=logger, file=file_path)

        rdv_mission21_processor = RDVProcessData(
            logger=logger,
            project=project,
            dataloader_class=mission21_dataloader,
            cho_class=RDVCHO_Mission21)
        # Map data (using google spreadsheets, write results back to google drive)
        mission21_data = rdv_mission21_processor.map_data()
        rdv_mission21_processor.ingest_data2es(mission21_data)
        updates_processing.write_last_run_data(project)

    elif project == "stph-images" \
            or project == "mkb-images" or project == "foerster-images" \
            or project == "babarchive-images" or project == "bablibrary-images":
        collection_name = ap_config["datasets"][project]["media_server_collection"]
        folder = updates_processing.get_digital_objects_folder(project=project)
        image_processor.import_images(folder, collection_name, project)
        updates_processing.write_last_run_data(project)
    logger.handlers[0].flush()  # make sure that the email is sent after each project
