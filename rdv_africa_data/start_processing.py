import os

import yaml
from pkg_resources import Requirement, resource_filename

from rdv_hierarchy_ubit import BABThesaurusLoader
from rdv_africa_data.data_loader.rdv_mkb import RDVDataMKB, RDVCHO_MKB
from rdv_africa_data.data_loader.rdv_swisstph import RDVDataSwissTPH, RDVCHOSwissTPH, RDVCHOSwissTPHProjects, \
    RDVDataSwissTPHProjects, RDVDataSwissTPHFilms, RDVCHOSwissTPHFilms
from rdv_africa_data.data_process import RDVProcessData

from rdv_africa_data.data_loader import RDVCHO_BSIZ, UBSAfricaOAI, RDVDataBABArchive
from rdv_africa_data.data_loader import RDVDataBABLibrary, RDVCHO_BAB

from rdv_data_helpers_ubit import get_rdv_logger

from rdv_marc_ubit import AlmaIZMarcJSONRecord, MarcTransformRule

from cache_decorator_redis_ubit import NoCacheDecorator
from mongo_decorator_ubit import create_mongo_decorator

# from libxmp.utils import file_to_dict
# from libxmp import XMPFiles, consts

# projects = ["mkb", "bablibrary", "babarchive", "bsizdata", "stph", "stph_projects", "stph_films", "babthesaurus"]
projects = ["bsizdata"]

# for kubernetes
config_file = "config/ap_config.yaml"

# for local development
if os.getenv("RDV_DEVELOPMENT") == "1":
    config_file = "config/ap_config_local.yaml"
    # projects = ["babarchive"]
    projects = ["babarchive"]


with open(config_file) as ap_config_fh:
    ap_config = yaml.load(ap_config_fh, Loader=yaml.BaseLoader)

gd_service_account_environment_variable = ap_config.get("google_drive", {}).get("service_account_environment_variable")
marc_spreadsheet = ap_config.get("datasets", {}).get("bsizdata", {}).get("process", {}).get("marc_process_spreadsheet")

for project in projects:
    if project == "bsizdata":
        logger = get_rdv_logger(project)
        # get Transformation Rule for Marc-Records
        marc_rule = MarcTransformRule(
            marc_spreadsheet=marc_spreadsheet,
            gd_service_file="",
            gd_service_account_env_var=gd_service_account_environment_variable,
            marc_record_class=AlmaIZMarcJSONRecord,
            gd_cache_decorator=NoCacheDecorator)
        # create Decorator for Mongo to store OAI-Records

        # when it runs via docker-compose, the host name for mongo is not localhost but mongo
        # TODO improve this (either expose port via docker-compose config or take this in config)
        if os.getenv("MODE") == "container":
            mongo_config = {"host": "mongo"}
        else:
            mongo_config = {"host": "mongodb://localhost"}

        MongoDecorator = create_mongo_decorator(config_dict=mongo_config)
        # specify database and collection
        slsp_oai_store = MongoDecorator(db_name="slsp_oai_harvesting", collection_name="africa")
        # build oai Harvester for SLSP africa-set
        # ES tunnel for port to ingest data into ES-> localhost:9209
        # incremental=True would only harvest new / changed records
        # incremental=False & Reharvest=False, would load old records from cache and new records
        # since last_harvest_date or if not set
        # from last stored date for harvesting from mongo db
        # reharvest=True: would reharvest the whole data_set and overwrite all
        # data within in mongo db

        # Initialize OAI Harvester
        ubsafrica_oai_harvester = UBSAfricaOAI(
            oai_set="africa",
            oai_metadata="marc21",
            oai_sets={},
            cachedecorator=NoCacheDecorator,
            oai_store=slsp_oai_store,
            marc_rule=marc_rule,
            logger=logger,
            es_host="this-is-not-used",
            incremental=True,
            reharvest=False,
            last_harvest_date=ap_config.get("datasets", {}).
                    get("bsizdata", {}).get("process", {}).get("last_harvest_date"))

        # Harvest data
        rdv_bsizdata_processor = RDVProcessData(
            logger=logger,
            config_file=config_file,
            project=project,
            dataloader_class=ubsafrica_oai_harvester,
            cho_class=RDVCHO_BSIZ,
            gd_cachedecorator=NoCacheDecorator)
        # Map data (using google spreadsheets, write results back to google drive)
        bsiz_data = rdv_bsizdata_processor.map_data()
        # show mapped data
        # print(bsiz_data)
        # TODO: incremental ingest Parameter needs to be passed, can be done via ap_config.yaml, without passing incremental=True only full ingest possible
        # ingest data in elasticsearch
        rdv_bsizdata_processor.ingest_data2es(bsiz_data)
    elif project == "mkb":
        logger = get_rdv_logger(project)

        # Initialize RDVDataMKB

        file_path = ap_config["datasets"][project]["process"]["file"]

        mkb_dataloader = RDVDataMKB(logger=logger, file=file_path)

        rdv_mkb_processor = RDVProcessData(
            logger=logger,
            config_file=config_file,
            project=project,
            dataloader_class=mkb_dataloader,
            cho_class=RDVCHO_MKB,
            gd_cachedecorator=NoCacheDecorator)
        # Map data (using google spreadsheets, write results back to google drive)
        mkb_data = rdv_mkb_processor.map_data()
        rdv_mkb_processor.ingest_data2es(mkb_data)
    elif project == "bablibrary":
        logger = get_rdv_logger(project)
        folder = ap_config["datasets"][project]["process"]["folder"]
        thesaurus_path = ap_config["datasets"][project]["process"]["thesaurus_filename"]
        img_lookup = ap_config["datasets"][project]["process"]["img_lookup"]

        bab_library_dataloader = RDVDataBABLibrary(logger=logger, folder=folder, thesaurus_filename=thesaurus_path, img_lookup=img_lookup)
        rdv_bablibrary_processor = RDVProcessData(
            logger=logger,
            config_file=config_file,
            project=project,
            dataloader_class=bab_library_dataloader,
            cho_class=RDVCHO_BAB,
            gd_cachedecorator=NoCacheDecorator)
        bablibrary_data = rdv_bablibrary_processor.map_data()

        rdv_bablibrary_processor.ingest_data2es(bablibrary_data)

    elif project == "babarchive":
        logger = get_rdv_logger(project)
        folder = ap_config["datasets"][project]["process"]["folder"]
        thesaurus_path = ap_config["datasets"][project]["process"]["thesaurus_filename"]
        img_lookup = ap_config["datasets"][project]["process"]["img_lookup"]

        bab_archive_dataloader = RDVDataBABArchive(logger=logger, folder=folder, thesaurus_filename=thesaurus_path, img_lookup=img_lookup)
        rdv_babarchive_processor = RDVProcessData(
            logger=logger,
            config_file=config_file,
            project=project,
            dataloader_class=bab_archive_dataloader,
            cho_class=RDVCHO_BAB,
            gd_cachedecorator=NoCacheDecorator)
        babarchive_data = rdv_babarchive_processor.map_data()

        rdv_babarchive_processor.ingest_data2es(babarchive_data)
    elif project == "stph":
        logger = get_rdv_logger(project)

        file_path = ap_config["datasets"][project]["process"]["file"]

        stph_dataloader = RDVDataSwissTPH(logger=logger, file=file_path)

        rdv_stph_processor = RDVProcessData(
            logger=logger,
            config_file=config_file,
            project=project,
            dataloader_class=stph_dataloader,
            cho_class=RDVCHOSwissTPH,
            gd_cachedecorator=NoCacheDecorator)
        # Map data (using google spreadsheets, write results back to google drive)
        stph_data = rdv_stph_processor.map_data()
        rdv_stph_processor.ingest_data2es(stph_data)

    elif project == "stph_projects":
        logger = get_rdv_logger(project)
        projects_file_path = ap_config["datasets"][project]["process"]["file"]

        stph_dataloader_projects = RDVDataSwissTPHProjects(logger=logger, file=projects_file_path)

        rdv_stph_processor_projects = RDVProcessData(
            logger=logger,
            config_file=config_file,
            project=project,
            dataloader_class=stph_dataloader_projects,
            cho_class=RDVCHOSwissTPHProjects,
            gd_cachedecorator=NoCacheDecorator)
        # Map data (using google spreadsheets, write results back to google drive)
        stph_data_projects = rdv_stph_processor_projects.map_data()

        rdv_stph_processor_projects.ingest_data2es(stph_data_projects)
    elif project == "stph_films":
        logger = get_rdv_logger(project)
        films_file_path = ap_config["datasets"][project]["process"]["file"]

        stph_dataloader_films = RDVDataSwissTPHFilms(logger=logger, file=films_file_path)

        rdv_stph_processor_films = RDVProcessData(
            logger=logger,
            config_file=config_file,
            project=project,
            dataloader_class=stph_dataloader_films,
            cho_class=RDVCHOSwissTPHFilms,
            gd_cachedecorator=NoCacheDecorator)
        # Map data (using google spreadsheets, write results back to google drive)
        stph_data_films = rdv_stph_processor_films.map_data()

        rdv_stph_processor_films.ingest_data2es(stph_data_films)

    # elif project == "foerster":
        # small attempt to extract metadata from new foerster pictures
        # files = [
        #     "/home/lionel/Data/arbim/afrika-portal/FOERSTER/20200117_9337b.psd",
        #     "/home/lionel/Data/arbim/afrika-portal/FOERSTER/2022_01_15_2667.psd",
        #     "/home/lionel/Data/arbim/afrika-portal/FOERSTER/5.0-241b.psd",
        #     "/home/lionel/Data/arbim/afrika-portal/FOERSTER/82_16_0008b.psd",
        #     "/home/lionel/Data/arbim/afrika-portal/FOERSTER/83-26-0005b.psd"
        # ]
        # for file in files:
        #     # xmp_file = XMPFiles(file_path=file, open_forupdate=True)
        #     # xmp = xmp_file.get_xmp()
        #
        #     # print(xmp.get_property(consts.XMP_NS_DC, 'format'))
        #     # print(xmp.get_property(consts.XMP_NS_DC, 'title[1]'))
        #
        #
        #     xmpdict = file_to_dict(file)
        #     # print(xmpdict)
        #     dc = xmpdict[consts.XMP_NS_DC]
        #
        #     print("\n\n")
        #     print(file)
        #     print("--------------------------------------------")
        #     for i in range(0,15):
        #         if(not("lang" in dc[i][0])):
        #             print(dc[i][0] + ":")
        #             print(dc[i][1])
        #
        #     lr = xmpdict[consts.XMP_NS_Lightroom]
        #     for i in range(0,5):
        #         if(not("lang" in dc[i][0])):
        #             print(lr[i][0] + ":")
        #             print(lr[i][1])
        #
        #     # print(xmp)

    elif project == "babthesaurus":
        logger = get_rdv_logger(project)
        thesaurus_es_host = ap_config["elasticsearch"]["es_host"]
        thesaurus_xml_file = ap_config["datasets"][project]["process"]["thesaurus_filename"]
        thesaurus_index = ap_config["datasets"][project]["ingest"]["index"]
        index_prefix = ap_config["datasets"][project]["ingest"]["index_prefix"]
        bab_thesaurus = BABThesaurusLoader(host=thesaurus_es_host, index=thesaurus_index, index_prefix=index_prefix, logger=logger, file=thesaurus_xml_file, incremental=False)
        bab_thesaurus.update_es_hierarchy()
