import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="rdv_africa_data",
    version="0.0.6",
    author="Martin Reisacher",
    author_email="martin.reisacher@unibas.ch",
    description="build index for africaslsp data",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    # package_dir={"": "ubunibas_loggers"},
    packages=setuptools.find_packages(),
    install_requires=['cache-decorator-redis-ubit', 'mongo-decorator-ubit==0.1.5', 'es-ingester-ubit', 'elasticsearch<8',
                      'values-normalizer-ubit',
                      'gdspreadsheets-ubit',
                      'rdv_data_helpers_ubit', 'rdv_marc_ubit',
                      'digi_oai_eportal_ubit==0.0.6', 'pyyaml', 'pandas', 'rdv_hierarchy_ubit', 'python-xmp-toolkit', 'requests'],
    python_requires=">=3.6",
    include_package_data=True,
    zip_safe=False
)
