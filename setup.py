import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="rdv_africa_data",
    version="0.0.6",
    author="Martin Reisacher",
    author_email="martin.reisacher@unibas.ch",
    description="build index for africaslsp data",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    # package_dir={"": "ubunibas_loggers"},
    packages=setuptools.find_packages(),
    install_requires=[
        "elasticsearch<8",
        "pandas<3",
        "python-xmp-toolkit<3",
        "pyyaml<7",
        "requests<3",
        "wand==0.6.13",

        # rdv modules"
        "cache-decorator-redis-ubit==0.2.5",
        "digi-oai-eportal-ubit==0.0.13",
        "es-ingester-ubit==0.1.2",
        "gdspreadsheets-ubit==0.1.7",
        "logging-decorator-ubit==0.1.3",
        "mongo-decorator-ubit==0.1.11",
        "rdv-data-helpers-ubit==0.2.4",
        "rdv-hierarchy-ubit==0.0.23",
        "rdv-marc-ubit==0.8.1",
        "values-normalizer-ubit==0.2.30",
    ],
    python_requires=">=3.6",
    include_package_data=True,
    zip_safe=False
)
