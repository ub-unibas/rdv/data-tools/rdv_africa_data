import os

import yaml
from pkg_resources import Requirement, resource_filename

from rdv_africa_data.data_process import RDVProcessData

from rdv_africa_data.data_loader import RDVCHO_BSIZ, UBSAfricaOAI

from rdv_data_helpers_ubit import get_rdv_logger

from rdv_marc_ubit import AlmaIZMarcJSONRecord, MarcTransformRule

from cache_decorator_redis_ubit import NoCacheDecorator
from mongo_decorator_ubit import create_mongo_decorator


def test_alma():
    config_file = "config/ap_config_test.yaml"
    with open(config_file) as ap_config_fh:
        ap_config = yaml.load(ap_config_fh, Loader=yaml.BaseLoader)

    gd_service_account_environment_variable = ap_config.get("google_drive", {}).get("service_account_environment_variable")
    marc_spreadsheet = ap_config.get("datasets", {}).get("bsizdata", {}).get("process", {}).get(
        "marc_process_spreadsheet")

    project = "bsizdata"
    logger = get_rdv_logger(project)

    if os.getenv("MODE") == "container":
        mongo_config = {"host": "mongo"}
    else:
        mongo_config = {"host": "localhost"}

    mongo_decorator = create_mongo_decorator(config_dict=mongo_config)

    slsp_oai_store = mongo_decorator(db_name="slsp_oai_harvesting", collection_name="africa")

    marc_rule = MarcTransformRule(
        marc_spreadsheet=marc_spreadsheet,
        gd_service_file="",
        gd_service_account_env_var=gd_service_account_environment_variable,
        marc_record_class=AlmaIZMarcJSONRecord,
        gd_cache_decorator=NoCacheDecorator)

    ubsafrica_oai_harvester = UBSAfricaOAI(
        oai_set="africa",
        oai_metadata="marc21",
        oai_sets={},
        cachedecorator=NoCacheDecorator,
        oai_store=slsp_oai_store,
        marc_rule=marc_rule,
        logger=logger,
        es_host="localhost:9209",
        incremental=True,
        reharvest=False,
        last_harvest_date="2022-06-25")

    rdv_bsizdata_processor = RDVProcessData(
        logger=logger,
        config_file=config_file,
        project=project,
        dataloader_class=ubsafrica_oai_harvester,
        cho_class=RDVCHO_BSIZ,
        gd_cachedecorator=NoCacheDecorator)

    bsiz_data = rdv_bsizdata_processor.map_data()

    assert(len(bsiz_data) > 50)
    assert(len(bsiz_data) < 2000)
    assert ("title" in bsiz_data["9914586340105504"])
    assert ("oai_id" in bsiz_data["9914586340105504"])
    assert ("072_a2" in bsiz_data["9914586340105504"])

    assert bsiz_data["9914586340105504"]["264_abc"] == [
        "St. Augustin bei Bonn. Verl. des Anthropos-Instituts. 1983"
    ]
