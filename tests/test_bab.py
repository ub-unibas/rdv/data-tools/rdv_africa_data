from rdv_africa_data.data_loader import utility


def test_merge_multiple_csv_files():
    result = utility.merge_multiple_csv_files(folder="data")
    # result = utility.merge_multiple_csv_files(folder="../rdv_africa_data/data/BAB_Library")
    # print(result)

    should_be = [
        {'Objektart': 'Buch', 'Signatur': 'UBH 234', 'Verfasser/in': 'Albert Einstein', 'Ort': '', 'New Field': ''},
        {'Objektart': 'Buch', 'Signatur': 'UBH 235', 'Verfasser/in': '', 'Ort': 'Bern', 'New Field': ''},
        {'Objektart': 'Buch', 'Signatur': 'UBH 235', 'Verfasser/in': 'Jean-Bernard', 'Ort': 'Bern',
         'New Field': 'test'},
        {'Objektart': 'Buch', 'Signatur': 'UBH 235', 'Verfasser/in': '', 'Ort': 'Bern', 'New Field': 'test2'}
    ]
    assert(result == should_be)
