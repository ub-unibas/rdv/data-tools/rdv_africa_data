from data_loader import RDVCHO_BAB


def test_extract_page_order():
    res = RDVCHO_BAB.extract_page_order("K02_page11.jpg")
    assert (res == 11)
    res = RDVCHO_BAB.extract_page_order("K02_page00121.jpg")
    assert (res == 121)
    res = RDVCHO_BAB.extract_page_order("K02_page001221app.jpg")
    assert (res == 1221)
    res = RDVCHO_BAB.extract_page_order("cover1.jpg")
    assert (res == -9999)
    res = RDVCHO_BAB.extract_page_order("rear3.jpg")
    assert (res == 10003)
