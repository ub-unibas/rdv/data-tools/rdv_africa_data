import os
import time

import requests

import random
import string

import yaml
from pkg_resources import Requirement, resource_filename

from config.config import get_config
from media_server.media_server_client import MediaServerClient
from rdv_africa_data.data_process import RDVProcessData

from rdv_africa_data.data_loader import RDVCHO_BSIZ, UBSAfricaOAI

from rdv_data_helpers_ubit import get_rdv_logger

from rdv_marc_ubit import AlmaIZMarcJSONRecord, MarcTransformRule

from cache_decorator_redis_ubit import NoCacheDecorator
from mongo_decorator_ubit import create_mongo_decorator


def test_media_server_client():
    os.environ["CONFIG_FILE"] = "ap_config_pytest.yaml"
    media_server_client = MediaServerClient()
    media_server_client.create_collection(collection="test", collection_description="collection for unit tests")
    # random new signature
    new_signature = ''.join(random.choices(string.ascii_letters + string.digits, k=8)).lower()
    media_server_client.post_image(
        image_uri="https://gitlab.switch.ch/ub-unibas/rdv/data-tools/rdv_africa_data/-/raw/main/tests/data/basler_afrika_logo.png?ref_type=heads",
        signature=new_signature,
        collection="test"
    )
    items = media_server_client.get_items(collection="test")
    is_posted_image_in_the_items_list = any(item.get("signature") == new_signature for item in items)
    assert is_posted_image_in_the_items_list

    time.sleep(3)

    config = get_config()

    image_url = config["media_server"]["host"] + "/test/" + new_signature + "/item"

    # Fetch the image
    response = requests.get(image_url, stream=True)
    response.raise_for_status()

    # Get the file size in bytes from the Content-Length header
    file_size = int(response.headers.get('Content-Length', 0))

    # Output the file size
    assert (file_size == 2390)
