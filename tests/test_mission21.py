from data_loader.rdv_mission21 import RDVDataMission21


def test_get_country_based_on_call_number():
    country = RDVDataMission21.get_country_based_on_call_number("D-30.22.074")
    assert (country == "Ghana")
    country = RDVDataMission21.get_country_based_on_call_number("C-30.54.030")
    assert (country is None)
    country = RDVDataMission21.get_country_based_on_call_number("FA-10.1a")
    assert (country == "Liberia")
