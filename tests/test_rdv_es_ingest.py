from rdv_africa_data.data_ingest.rdv_es_ingest import RDVIngest


def test_add_facet_data_as_nested_field():
    data_simple = {
        "6c257fca-328c-55dc-9685-985e1d54e478": {
            "fct_mediatype": [
                "Bestände"
            ],
            "Objektart": "Bestände",
            "field_title": [
                "Namibia. Presse-Archiv und Graue Literatur / Press Archives and Ephemera"
            ],
            "fct_person_organisation": [
                "Nujoma, Sam",
                "Pohamba, Lucas Hifikepuye"
            ]
        }
    }

    result = RDVIngest.add_facet_data_as_nested_field(data_simple)

    should_be = {
        "6c257fca-328c-55dc-9685-985e1d54e478": {
            "fct_mediatype": [
                "Bestände"
            ],
            "Objektart": "Bestände",
            "field_title": [
                "Namibia. Presse-Archiv und Graue Literatur / Press Archives and Ephemera"
            ],
            "fct_person_organisation": [
                "Nujoma, Sam",
                "Pohamba, Lucas Hifikepuye"
            ],
            "keyword_facets": [
                {
                    "facet_name": "fct_mediatype",
                    "facet_value": "Bestände"
                },
                {
                    "facet_name": "fct_person_organisation",
                    "facet_value": "Nujoma, Sam"
                },
                {
                    "facet_name": "fct_person_organisation",
                    "facet_value": "Pohamba, Lucas Hifikepuye"
                }
            ]
        }
    }
    assert (result == should_be)
