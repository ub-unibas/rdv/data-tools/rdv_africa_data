from data_loader.rdv_swisstph import RDVDataSwissTPH
from rdv_data_helpers_ubit import get_rdv_logger


def test_get_item():
    logger = get_rdv_logger("test")
    tph_data = RDVDataSwissTPH(logger=logger, file="nothing")
    result = tph_data.get_list_of_available_images(collection="stph-images-2023-05-04-2")
    print(result)
