from rdv_africa_data.data_loader import utility


def test_merge_multiple_csv_files():
    result = utility.merge_multiple_csv_files(folder="data")
    # result = utility.merge_multiple_csv_files(folder="../rdv_africa_data/data/BAB_Library")
    # print(result)

    should_be = [
        {'Objektart': 'Buch', 'Signatur': 'UBH 234', 'Verfasser/in': 'Albert Einstein', 'Ort': '', 'New Field': ''},
        {'Objektart': 'Buch', 'Signatur': 'UBH 235', 'Verfasser/in': '', 'Ort': 'Bern', 'New Field': ''},
        {'Objektart': 'Buch', 'Signatur': 'UBH 235', 'Verfasser/in': 'Jean-Bernard', 'Ort': 'Bern',
         'New Field': 'test'},
        {'Objektart': 'Buch', 'Signatur': 'UBH 235', 'Verfasser/in': '', 'Ort': 'Bern', 'New Field': 'test2'}
    ]
    assert(result == should_be)


def test_merge_multiple_csv_files_based_on_id():
    result = utility.merge_multiple_csv_files_based_on_id(sheet_left="data/sheet4.csv", sheet_right="data/sheet5.csv", common_column_name_left="Id", common_column_name_right="Id")

    should_be = [
        {'Id': '1', 'Signatur': 'UBH 235', 'Ort': 'Bern', 'Verfasser/in': 'Jean-Bernard', 'Material': 'Wolle', 'Land': 'Ghana'},
        {'Id': '2', 'Signatur': 'UBH 236', 'Ort': 'Bern', 'Verfasser/in': 'Fred', 'Material': 'Eisen', 'Land': 'Nigeria'},
        {'Id': '3', 'Signatur': 'UBH 237', 'Ort': 'Genf', 'Verfasser/in': '', 'Material': 'Blech', 'Land': ''},
    ]
    assert(result == should_be)
